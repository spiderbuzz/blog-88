<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Blog-88 Homepage Three</title>

     <!--Bootstrap CSSS-->
    <link rel="stylesheet" href="css/bootstrap.min.css"> 
    <!--Google fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=DM+Serif+Display|Poppins&display|Roboto&display=swap">
    <link rel="stylesheet" href="font-awesome/css/fontawsome.css">
    <link href="https://fonts.googleapis.com/css?family=Kristi&display=swap" rel="stylesheet">
    <link rel="icon" type="text/css" href="">
    <link rel="stylesheet" href="library/owl-carousel/css/owl.carousel.min.css">  
    <link rel="stylesheet" href="library/slick/slick.css">  
    <link rel="stylesheet" href="css/animate.css">  
    <link rel="stylesheet" href="css/style.css">  
</head>
<body>

    <!--header starts--> 
    <header class="top-header homepage-three">
        <div class="container">
            <div class="nav-top-wrapper">
                <div class="row">
                    <div class="col-4">
                        <ul class="footer-links-list top-header-third">
                            <li class="footer-list d-inline-block mr-3">
                                <div class="footer-two-icons">
                                    <a href="" class="footer-icon-link active">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </div>
                            </li>
                            <li class="footer-list d-inline-block mr-3">
                                <div class="footer-two-icons">
                                    <a href="" class="footer-icon-link">
                                        <i class="fab fa-youtube"></i>
                                    </a>
                                </div>
                            </li>
                            <li class="footer-list d-inline-block mr-3">
                                <div class="footer-two-icons">
                                    <a href="" class="footer-icon-link">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </div>
                            </li>
                            <li class="footer-list d-inline-block mr-3">
                                <div class="footer-two-icons">
                                    <a href="" class="footer-icon-link">
                                        <i class="fab fa-instagram"></i>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-4">
                        <div class="homepage-three-link">
                            <a class="navbar-brand" href="">Blog88</a>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="dialer-section">
                            <li class="dial-icon">
                                <a href="" class="dialer-icon">
                                    <i class="fas fa-phone"></i>
                                </a>
                            </li>
                            <li class="dial-icon number">
                                <a href="">call@blog88</a>
                            </li>
                        </div>
                    </div>
                </div>
            </div>
            <nav class="navbar navbar-expand-lg navbar-light">
                <a class="navbar-brand tablet-view" href="#">
                    <img src="images/mennu-two.png" alt="">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icons">
                        <img src="images/mennu-two.png" alt="">
                    </span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav homepage-navbar mr-auto ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Business <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">World </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Fashion </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Food </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Style </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Football </a>
                        </li>                 
                    </ul>
                    <form class="form-inline my-2 my-lg-0 header-third-search">
                        <input class="form-control mr-sm-2 d-none" type="search" placeholder="Search" aria-label="Search">
                        <button class="btn search-button-header my-2 my-sm-0 pr-0" type="submit">
                            <i class="fas fa-search"></i>
                        </button>
                    </form>
                    <div class="fifth-layout-mobile">
                        <ul class="footer-links-list top-header-third">
                            <li class="footer-list d-inline-block mr-3">
                                <div class="footer-two-icons">
                                    <a href="" class="footer-icon-link active">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </div>
                            </li>
                            <li class="footer-list d-inline-block mr-3">
                                <div class="footer-two-icons">
                                    <a href="" class="footer-icon-link">
                                        <i class="fab fa-youtube"></i>
                                    </a>
                                </div>
                            </li>
                            <li class="footer-list d-inline-block mr-3">
                                <div class="footer-two-icons">
                                    <a href="" class="footer-icon-link">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </div>
                            </li>
                            <li class="footer-list d-inline-block mr-3">
                                <div class="footer-two-icons">
                                    <a href="" class="footer-icon-link">
                                        <i class="fab fa-instagram"></i>
                                    </a>
                                </div>
                            </li>
                        </ul>
                        <form class="form-inline my-2 my-lg-0">
                            <input class="form-control mr-sm-2 d-none" type="search" placeholder="Search" aria-label="Search">
                            <button class="btn search-button-header my-2 my-sm-0 pr-0" type="submit">
                                <i class="fas fa-search"></i>
                            </button>
                        </form> 
                    </div>
                </div>
            </nav>
            <div class="fifth-header-layout">
                <div class="homepage-three-link">
                    <a class="navbar-brand" href="">Blog88</a>
                </div>
            </div>
        </div>        
    </header>
    <!--header ends--> 

