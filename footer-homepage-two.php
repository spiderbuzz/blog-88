 <div class="footer-two-wrapper wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
    <footer class="footer-homapage-two">
        <div class="container">
            <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-4 col-12">
                    <div class="footer-list-header">
                        <a class="footer-header-link" href="">about Blog88</a>
                        <p class="footer-links mb-0 pr-5 mr-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex</p>
                    </div>
                    <div class="instagram-section">
                        <a class="footer-header-link" href="">instagram</a>
                        <div class="insta-sec-wrapper">
                            <div class="row">
                                <div class="col-4 mid-column">
                                    <div class="footer-img">
                                        <img src="images/denim-lady.jpg" alt="">
                                    </div>
                                </div>
                                <div class="col-4 mid-column">
                                    <div class="footer-img">
                                        <img src="images/girl-ballooon.jpg" alt="">
                                    </div>
                                </div>
                                <div class="col-4 mid-column">
                                    <div class="footer-img">
                                        <img src="images/lady-hat.jpg" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-4 mid-column">
                                    <div class="footer-img">
                                        <img src="images/vegetables.jpg" alt="">
                                    </div>
                                </div>
                                <div class="col-4 mid-column">
                                    <div class="footer-img">
                                        <img src="images/curly-hair.jpg" alt="">
                                    </div>
                                </div>
                                <div class="col-4 mid-column">
                                    <div class="footer-img">
                                        <img src="images/boy-alon.jpg" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-4 mid-column">
                                    <div class="footer-img">
                                        <img src="images/emo.jpg" alt="">
                                    </div>
                                </div>
                                <div class="col-4 mid-column">
                                    <div class="footer-img">
                                        <img src="images/vacation.jpg" alt="">
                                    </div>
                                </div>
                                <div class="col-4 mid-column">
                                    <div class="footer-img">
                                        <img src="images/raft.jpg" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="footer-insta-button">
                                <div class="read-more-btn">
                                    <a href="#" class="btn after-btn">
                                        <span class="angle-button">@blog88</span>
                                        <img class="angle-img" src="images/arrow-right.png" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-6">
                    <div class="blog-date-wrap">
                        <div class="blog-date-header">
                            <a class="footer-header-link" href="">today's</a>
                        </div>
                        <div class="recent-post-click first-child">
                            <div class="recent-image">
                                <img src="images/lady-hat.jpg" alt="">
                            </div>
                            <div class="about-recent-head">
                                <div class="recent-wrap in-it">
                                    <span class="recent-line">July 29, 2019</span>
                                    <a href="" class="recent-head">The Beauty Of Sunset Create A Romantic</a>
                                </div>
                            </div>                                       
                        </div>
                        <div class="recent-post-click">
                            <div class="recent-image">
                                <img src="images/denim-lady.jpg" alt="">
                            </div>
                            <div class="about-recent-head">
                                <div class="recent-wrap in-it">
                                    <span class="recent-line">July 29, 2019</span>
                                    <a href="" class="recent-head">The Beauty Of Sunset Create A Romantic</a>
                                </div>
                            </div>                                       
                        </div>
                    </div>
                    <div class="blog-date-wrap second-row">
                        <div class="blog-date-header">
                            <a class="footer-header-link" href="">yesterday's</a>
                        </div>
                        <div class="recent-post-click first-child">
                            <div class="recent-image">
                                <img src="images/lady-hat.jpg" alt="">
                            </div>
                            <div class="about-recent-head">
                                <div class="recent-wrap in-it">
                                    <span class="recent-line">July 29, 2019</span>
                                    <a href="" class="recent-head">The Beauty Of Sunset Create A Romantic</a>
                                </div>
                            </div>                                       
                        </div>
                        <div class="recent-post-click">
                            <div class="recent-image">
                                <img src="images/denim-lady.jpg" alt="">
                            </div>
                            <div class="about-recent-head">
                                <div class="recent-wrap in-it">
                                    <span class="recent-line">July 29, 2019</span>
                                    <a href="" class="recent-head">The Beauty Of Sunset Create A Romantic</a>
                                </div>
                            </div>                                       
                        </div>
                        <div class="recent-post-click">
                            <div class="recent-image">
                                <img src="images/denim-lady.jpg" alt="">
                            </div>
                            <div class="about-recent-head">
                                <div class="recent-wrap in-it">
                                    <span class="recent-line">July 29, 2019</span>
                                    <a href="" class="recent-head"> The Beauty Of Sunset Create A Romantic</a>
                                </div>
                            </div>                                       
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 col-6">
                    <div class="footer-head-wrapper">
                        <a class="footer-header-link" href="">nav links</a>
                    </div>
                    <ul class="footer-links-list">
                        <li class="footer-list">
                            <a href="" class="footer-links">Home</a>
                        </li>
                        <li class="footer-list">
                            <a href="" class="footer-links">All Products</a>
                        </li>
                        <li class="footer-list">
                            <a href="" class="footer-links">Shop</a>
                        </li>
                        <li class="footer-list">
                            <a href="" class="footer-links">Top Products</a>
                        </li>
                        <li class="footer-list">
                            <a href="" class="footer-links">Pages / Layouts</a>
                        </li>
                    </ul>
                    <div class="quick-links">
                        <div class="footer-head-wrapper">
                            <a class="footer-header-link" href="">quick links</a>
                        </div>
                        <ul class="footer-links-list">
                            <li class="footer-list">
                                <a href="" class="footer-links">Home</a>
                            </li>
                            <li class="footer-list">
                                <a href="" class="footer-links">Our Features</a>
                            </li>
                            <li class="footer-list">
                                <a href="" class="footer-links">Our Benefits</a>
                            </li>
                            <li class="footer-list">
                                <a href="" class="footer-links">Gallery</a>
                            </li>
                            <li class="footer-list">
                                <a href="" class="footer-links">Latest News</a>
                            </li>
                        </ul>
                    </div>
                    <div class="quick-links social-medias">
                        <div class="footer-head-wrapper">
                            <a class="footer-header-link" href="">find social</a>
                        </div>
                        <ul class="footer-links-list">
                            <li class="footer-list d-inline-block mr-3">
                                <div class="footer-two-icons">
                                    <a href="" class="footer-icon-link active">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </div>
                            </li>
                            <li class="footer-list d-inline-block mr-3">
                                <div class="footer-two-icons">
                                    <a href="" class="footer-icon-link">
                                        <i class="fab fa-youtube"></i>
                                    </a>
                                </div>
                            </li>
                            <li class="footer-list d-inline-block mr-3">
                                <div class="footer-two-icons">
                                    <a href="" class="footer-icon-link">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </div>
                            </li>
                            <li class="footer-list d-inline-block mr-3">
                                <div class="footer-two-icons">
                                    <a href="" class="footer-icon-link">
                                        <i class="fab fa-instagram"></i>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="terms-list">
                        <div class="terms-list-links">
                            <a class="footer-links">FAQ</a>
                            <a class="footer-links">Terms and Conditions</a>
                            <a class="footer-links">Privacy Policy</a>
                            <a class="footer-links">About</a>
                            <a class="footer-links">Career</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
 </div>

 <div class="footer-copy-right">
    <h1 class="copy-right-section mb-0">Blogger 2018 | All Rights Reserved, Design & Developed by Themerelic</h1>
 </div>
 
 
 
 
 
 
 
 <!-- Java JavaScript -->
    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/bootstrap.bundle.min.js" ></script>
    <script src="library/owl-carousel/js/owl.carousel.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/app.js"></script>



    
</body>
</html>