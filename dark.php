<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Blog-88 Dark</title>

     <!--Bootstrap CSSS-->
    <link rel="stylesheet" href="css/bootstrap.min.css"> 
    <!--Google fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=DM+Serif+Display|Poppins&display|Roboto&display=swap">
    <link rel="stylesheet" href="font-awesome/css/fontawsome.css">
    <link href="https://fonts.googleapis.com/css?family=Kristi&display=swap" rel="stylesheet">
    <link rel="icon" type="text/css" href="">
    <link rel="stylesheet" href="library/owl-carousel/css/owl.carousel.min.css">  
    <link rel="stylesheet" href="css/animate.css">  
    <link rel="stylesheet" href="css/style.css">  
</head>
<body>

    <!--header starts--> 
    <header class="top-header">
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light navbar-fixed">
                <a class="navbar-brand" href="#">Blog-88</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-icon">
                        <i class="open-nav fas fa-bars"></i>
                    </span>
                </button>
                
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Business <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="#">World </a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Fashion </a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Food </a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Style </a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Football </a>
                        </li>                 
                    </ul>
                    <form class="form-inline my-2 my-lg-0">
                        <input class="form-control top-header-search mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                        <button class="btn my-2 my-sm-0" type="submit">
                            <i class="fas fa-search"></i>
                        </button>
                    </form>
                </div>
            </nav>
        </div>        
    </header>
    <!--header ends--> 

    <!--mid-banner starts-->
    <?php include("mid-carousel-dark.php") ; ?>
    <!--mid-banner ends-->

    <!--read-more section starts -->

     <div class="read-more-wrapper">
         <div class="container">
             <div class="read-wrap">
                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                        <div class="card wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                            <div class="card-header-sec">
                                <img class="more-wrap-image" src="images/food.jpg" alt="">
                                <div class="horizontal-button">
                                    <div class="continue-reading-btn">
                                        <a href="#" class="btn after-btn">
                                            <span class="angle-button">Read more</span>
                                            <img class="angle-img" src="images/arrow-right.png" alt="">
                                        </a>
                                    </div>
                                </div>                                            
                            </div>                                    
                            <div class="card-body">
                                <span class="card-span">By Spiderbuzz &nbsp &nbsp    |  &nbsp  &nbsp  July 29, 2019</span>
                                <h5 class="card-title">The Beauty Of Sunset Create A Romantic Magic In The Hearts of</h5>
                                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>

                                <div class="like-button">
                                    <div class="read-more-like">
                                        <div class="btn-like-wrap">
                                            <div class="like-back">
                                                <i class="fas fa-heart"></i>
                                            </div> 
                                        </div>  
                                    </div>                                                                           
                                </div>                                                                  
                            </div>                            
                        </div>                                 
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 col-12 mobile-view-top">
                        <div class="card wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                            <div class="card-header-sec">
                                <img class="more-wrap-image" src="images/food.jpg" alt="">
                                <div class="horizontal-button">
                                    <div class="continue-reading-btn">
                                        <a href="#" class="btn after-btn">
                                            <span class="angle-button">Read more</span>
                                            <img class="angle-img" src="images/arrow-right.png" alt="">
                                        </a>
                                    </div>
                                </div>                                            
                            </div>                                    
                            <div class="card-body">
                                <span class="card-span">By Spiderbuzz &nbsp &nbsp    |  &nbsp  &nbsp  July 29, 2019</span>
                                <h5 class="card-title">The Beauty Of Sunset Create A Romantic Magic In The Hearts of</h5>
                                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>

                                <div class="like-button">
                                    <div class="read-more-like">
                                        <div class="btn-like-wrap">
                                            <div class="like-back">
                                                <i class="fas fa-heart"></i>
                                            </div> 
                                        </div>  
                                    </div>                                                                           
                                </div>                                                                   
                            </div>                            
                        </div>                                 
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 col-12 tablet-view">
                        <div class="card wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                            <div class="card-header-sec">
                                <img class="more-wrap-image" src="images/food.jpg" alt="">
                                <div class="horizontal-button">
                                    <div class="continue-reading-btn">
                                        <a href="#" class="btn after-btn">
                                            <span class="angle-button">Read more</span>
                                            <img class="angle-img" src="images/arrow-right.png" alt="">
                                        </a>
                                    </div>
                                </div>                                            
                            </div>                                       
                            <div class="card-body">
                                <span class="card-span">By Spiderbuzz &nbsp &nbsp    |  &nbsp  &nbsp  July 29, 2019</span>
                                <h5 class="card-title">The Beauty Of Sunset Create A Romantic Magic In The Hearts of</h5>
                                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>

                                <div class="like-button">
                                    <div class="read-more-like">
                                        <div class="btn-like-wrap">
                                            <div class="like-back">
                                                <i class="fas fa-heart"></i>
                                            </div> 
                                        </div>  
                                    </div>                                                                           
                                </div>                                                                   
                            </div>                            
                        </div>                                 
                    </div>
                </div>                
             </div>             
         </div>
     </div>

    <!--read-more section ends -->

    <!--blog-like section starts -->

    <div class="blog-like">
        <div class="container">
            <div class="blog-read">
                <div class="row">
                    <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12">
                        <div class="image-like wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                            <img class="read-blog-img" src="images/dark-city.jpg" alt="">
                            <div class="like-button">
                                <div class="read-more-like">
                                    <div class="btn-like-wrap">
                                        <div class="like-back">
                                            <i class="fas fa-heart"></i>
                                        </div> 
                                    </div>  
                                </div>                                                                           
                            </div>    
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-7 col-md-7 col-sm-12">
                        <div class="like-image-desp wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                            <div class="two-content">
                                <div class="beauty-head">
                                    <span class="top-span">By Spiderbuzz &nbsp &nbsp    |  &nbsp  &nbsp  July 29, 2019</span>
                                    <h1 class="white-head">The Beauty Of Sunset Create A Romantic Magic In The Hearts Of Lovers and a Cute Baby In Future</h1>
                                    <div class="like-comment">
                                        <ul class="like-link">
                                            <li class="like-item">
                                                <i class="fas fa-heart"></i>
                                                <span>568</span>
                                                <a>Likes</a>
                                            </li>
                                            <li class="like-item">
                                                <i class="fas fa-comments"></i>
                                                <span>268</span>
                                                <a>Comments</a>
                                            </li>
                                            <li class="like-item">
                                                <i class="fas fa-share"></i>
                                                <span>10</span>
                                                <a>Shares</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo 
                                        consequat. </p>
                                    <div class="button-social-media">
                                        <div class="continue-reading-btn">
                                            <a href="#" class="btn after-btn">
                                                <span class="angle-button">Read more</span>
                                                <img class="angle-img" src="images/arrow-right.png" alt="">
                                            </a>
                                        </div>
                                        <div class="news-media">
                                            <ul class="news-item">
                                                <li class="news-link">
                                                    <a href=""><i class="fab fa-facebook-f"></i></a>
                                                </li>
                                                <li class="news-link">
                                                    <a href=""><i class="fab fa-youtube"></i></a>
                                                </li>
                                                <li class="news-link">
                                                    <a href=""><i class="fab fa-twitter"></i></a>
                                                </li>
                                                <li class="news-link">
                                                    <a href=""><i class="fab fa-instagram"></i></a>
                                                </li>
                                            </ul>                                        
                                        </div>  
                                    </div>                                                                          
                                </div>
                            </div>                   
                        </div>
                    </div>
                </div>
            </div>
            <div class="blog-read second-blog wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                <div class="row">
                    <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12">
                        <div class="image-like">
                            <img class="read-blog-img" src="images/dark-city.jpg" alt="">
                            <div class="like-button">
                                <div class="read-more-like">
                                    <div class="btn-like-wrap">
                                        <div class="like-back">
                                            <i class="fas fa-heart"></i>
                                        </div> 
                                    </div>  
                                </div>                                                                           
                            </div>    
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-7 col-md-7 col-sm-12">
                        <div class="like-image-desp">
                            <div class="two-content">
                                <div class="beauty-head">
                                    <span class="top-span">By Spiderbuzz &nbsp &nbsp    |  &nbsp  &nbsp  July 29, 2019</span>
                                    <h1 class="white-head">The Beauty Of Sunset Create A Romantic Magic In The Hearts Of Lovers and a Cute Baby In Future</h1>
                                    <div class="like-comment">
                                        <ul class="like-link">
                                            <li class="like-item">
                                                <i class="fas fa-heart"></i>
                                                <span>568</span>
                                                <a>Likes</a>
                                            </li>
                                            <li class="like-item">
                                                <i class="fas fa-comments"></i>
                                                <span>268</span>
                                                <a>Comments</a>
                                            </li>
                                            <li class="like-item">
                                                <i class="fas fa-share"></i>
                                                <span>10</span>
                                                <a>Shares</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo 
                                        consequat. </p>
                                    <div class="button-social-media">
                                        <div class="continue-reading-btn">
                                            <a href="#" class="btn after-btn">
                                                <span class="angle-button">Read more</span>
                                                <img class="angle-img" src="images/arrow-right.png" alt="">
                                            </a>
                                        </div>
                                        <div class="news-media">
                                            <ul class="news-item">
                                                <li class="news-link">
                                                    <a href=""><i class="fab fa-facebook-f"></i></a>
                                                </li>
                                                <li class="news-link">
                                                    <a href=""><i class="fab fa-youtube"></i></a>
                                                </li>
                                                <li class="news-link">
                                                    <a href=""><i class="fab fa-twitter"></i></a>
                                                </li>
                                                <li class="news-link">
                                                    <a href=""><i class="fab fa-instagram"></i></a>
                                                </li>
                                            </ul>                                        
                                        </div>  
                                    </div>                                                                          
                                </div>
                            </div>                   
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--blog-like section ends -->

    <!--blog-list-dark-->

    <section class="bloglist-dark-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                    <div class="blog-list-sec wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">                        
                        <div class="card mid-card">
                            <div class="blog-card-head">
                                <img class="column-divided-img" src="images/gentlemens.jpg" alt="">
                                <div class="card-first-btn">
                                    <a href="#" class="display-list-blog ">Travel</a>
                                </div>                                                                    
                                <div class="continue-reading-btn dark-theme-button">
                                    <a href="#" class="btn after-btn">
                                        <span class="angle-button">Read more</span>
                                        <img class="angle-img" src="images/arrow-right.png" alt="">
                                    </a>
                                </div>                           
                            </div>                                                                                  
                            <div class="card-body">
                                <h5 class="card-title">The Beauty Of Sunset Create A Romantic Magic In The Hearts Of 
                                    Lovers and a Cute Baby </h5>
                                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut 
                                    enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                                    aliquip ex ea commodo consequat. </p>    
                                <div class="theme-by">
                                    <span>By Spiderbuzz &nbsp  &nbsp  |  &nbsp &nbsp    July 29, 2019</span>
                                </div>
                                <div class="blog-list-media">
                                    <ul class="mid-link">
                                        <li class="mid-social-item">
                                            <a class="social-link" href="">
                                                <i class="fab fa-facebook-f"></i>
                                            </a>
                                        </li>
                                        <li class="mid-social-item">
                                            <a class="social-link" href="">
                                                <i class="fab fa-youtube"></i>
                                            </a>
                                        </li>
                                        <li class="mid-social-item">
                                            <a class="social-link" class="social-link" href="">
                                                <i class="fab fa-twitter"></i>
                                            </a>
                                        </li>
                                        <li class="mid-social-item">
                                            <a class="social-link" class="social-link" href="">
                                                <i class="fab fa-instagram"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>                        
                            </div>
                        </div>
                    </div>
                    <div class="blog-list-sec sec-top wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">                      
                        <div class="card mid-card">
                            <div class="blog-card-head">
                                <img class="column-divided-img" src="images/gentlemens.jpg" alt="">
                                <div class="card-first-btn">
                                    <a href="#" class="display-list-blog ">Travel</a>
                                </div>                                                                    
                                <div class="continue-reading-btn dark-theme-button">
                                    <a href="#" class="btn after-btn">
                                        <span class="angle-button">Read more</span>
                                        <img class="angle-img" src="images/arrow-right.png" alt="">
                                    </a>
                                </div>                           
                            </div>                                                                                  
                            <div class="card-body">
                                <h5 class="card-title">The Beauty Of Sunset Create A Romantic Magic In The Hearts Of 
                                    Lovers and a Cute Baby </h5>
                                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut 
                                    enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                                    aliquip ex ea commodo consequat. </p>    
                                <div class="theme-by">
                                    <span>By Spiderbuzz &nbsp  &nbsp  |  &nbsp &nbsp    July 29, 2019</span>
                                </div>
                                <div class="blog-list-media">
                                    <ul class="mid-link">
                                        <li class="mid-social-item">
                                            <a class="social-link" href="">
                                                <i class="fab fa-facebook-f"></i>
                                            </a>
                                        </li>
                                        <li class="mid-social-item">
                                            <a class="social-link" href="">
                                                <i class="fab fa-youtube"></i>
                                            </a>
                                        </li>
                                        <li class="mid-social-item">
                                            <a class="social-link" class="social-link" href="">
                                                <i class="fab fa-twitter"></i>
                                            </a>
                                        </li>
                                        <li class="mid-social-item">
                                            <a class="social-link" class="social-link" href="">
                                                <i class="fab fa-instagram"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>                        
                            </div>
                        </div>
                    </div>                                       
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                    <div class="next-column dark-theme wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                        <div class="blog-search-list dark wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                            <h6>search</h6>
                            <div class="search-box dark-section">
                                <form class="form-inline row">
                                    <input class="form-control col-xl-7 col-lg-7 col-md-12 col-sm-12" type="search" placeholder="Search for blog" aria-label="Search">
                                    <button class="btn col-xl-4 col-lg-4 col-md-6 col-sm-12" type="submit">Search</button>
                                </form>
                            </div>
                        </div>
                        <div class="about-author wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                            <h1 class="header-author">about author</h1>
                            <div class="author-image">
                                <img src="images/author.jpg" alt="">
                            </div>
                            <div class="author-name">
                                <span class="author-name-tag dark-theme">Kierrra Astor Doe</span>
                                <p class="author-name-para">Lorem ipsum dolor sit amet, consectetur adipiscing 
                                elit, sed do eiusmod tempor incididunt </p>
                                <div class="continue-reading-btn">
                                    <a href="#" class="btn after-btn">
                                        <span class="angle-button">Read more</span>
                                        <img class="angle-img" src="images/arrow-right.png" alt="">
                                    </a>
                                </div>  
                            </div>
                            <div class="about-news wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                                <h1 class="news-letter">newsletter</h1>
                                <div class="suscribe-newsletter">
                                    <div class="news-media">
                                        <ul class="news-item">
                                            <li class="news-link">
                                                <a href=""><i class="fab fa-facebook-f"></i></a>
                                            </li>
                                            <li class="news-link">
                                                <a href=""><i class="fab fa-youtube"></i></a>
                                            </li>
                                            <li class="news-link">
                                                <a href=""><i class="fab fa-twitter"></i></a>
                                            </li>
                                            <li class="news-link">
                                                <a href=""><i class="fab fa-instagram"></i></a>
                                            </li>
                                        </ul>                                        
                                    </div>
                                    <div class="news-head">
                                        <h1 class="news-header">Subscribe our newsletter and we’ll get back to you</h1>
                                    </div>
                                    <div class="news-form">
                                        <form class="form-inline">
                                            <div class="form-group col-12">                                            
                                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Your email address.....">
                                            </div>
                                            <div class="continue-reading-btn news-letter-button col-12">
                                                <a href="#" class="btn after-btn">
                                                    <span class="angle-button">Read more</span>
                                                    <img class="angle-img" src="images/arrow-right.png" alt="">
                                                </a>
                                            </div>                                             
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>                      
                    </div>  
                </div>
            </div>
        </div>
    </section>

    <!--blog-list-dark-->

    <!-- pagination -->

    <section class="pagination-wrap">
        <div class="container">
            <nav aria-label="Page navigation example wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                <ul class="pagination">
                    <li class="page-item"><a class="page-link" href="#"><i class="fas fa-angle-left"></i></a></li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#"><i class="fas fa-angle-right"></i></a></li>
                </ul>
            </nav>
        </div>
    </section>

    <!-- pagination -->

    <!--dark-footer-->

    <section class="dark-footer-wrapper dark-theme">
        <div class="container">
            <div class="dark-border wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                <div class="row">
                    <div class="col-xl-4 col-lg-5 col-md-4 col-sm-12">
                        <ul class="footer-media">
                            <li class="footer-item">
                                <div class="footer-media-icons">
                                    <a href="" class="footer-link">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </div>
                            </li>
                            <li class="footer-item youtube">
                                <div class="footer-media-icons">
                                    <a href="" class="footer-link">
                                        <i class="fab fa-youtube"></i>
                                    </a>
                                </div>
                            </li>
                            <li class="footer-item instagram">
                                <div class="footer-media-icons">
                                    <a href="" class="footer-link">
                                        <i class="fab fa-instagram"></i>
                                    </a>
                                </div>
                            </li>
                            <li class="footer-item twitter">
                                <div class="footer-media-icons">
                                    <a href="" class="footer-link">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12">
                        <div class="foot-head">
                            <a href="">Blog88</a>
                        </div>
                    </div>
                    <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12">
                        <div class="footer-menu">
                            <ul class="menu-item">
                                <li class="menu">
                                    <a href="" class="menu-link">FAQS</a>
                                </li>
                                <li class="menu">
                                    <a href="" class="menu-link">Terms and Conditions</a></li>
                                <li class="menu">
                                    <a href="" class="menu-link">About</a></li>
                                <li class="menu">
                                    <a href="" class="menu-link">Privacy Policy</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
    </section>

    <!--dark-footer-->




     <!-- Java JavaScript -->
    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/bootstrap.bundle.min.js" ></script>
    <script src="library/owl-carousel/js/owl.carousel.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/app.js"></script>
    <script>
        $('.carousel').carousel({
        interval: 100000000
        })
    </script>
    

</body>
</html>