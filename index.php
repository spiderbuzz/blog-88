<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Blog 88 Homepage</title>

     <!--Bootstrap CSSS-->
    <link rel="stylesheet" href="css/bootstrap.min.css"> 
    <!--Google fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=DM+Serif+Display|Poppins&display|Roboto&display=swap">
    <link rel="stylesheet" href="font-awesome/css/fontawsome.css">
    <link href="https://fonts.googleapis.com/css?family=Kristi&display=swap" rel="stylesheet">
    <link rel="icon" type="text/css" href="">
    <link rel="stylesheet" href="css/animate.css">  
    <link rel="stylesheet" href="css/style.css">  
</head>
<body>

    <!--banner starts-->

    <section class="top-banner-section">
        <div class="top-banner-wrapper">
            <div class="container">
                <div id="mySidenav" class="sidenav">
                    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                    <div class="blog-head">
                        <a href="" class="head-blog">Blog 88</a>
                        <div class="blog-theme">blog theme</div>
                    </div>
                    <div class="blog-search">
                        <input class="form-control" type="text" placeholder="search blog" aria-label="Search">                        
                    </div> 
                    <div class="header-menu">
                        <div class="ul head-menu">
                            <div class="nav-line"></div>
                            <div class="head-list">
                                <li class="header-item">
                                    <a class="header-link" href="#">home</a>                         
                                </li>
                                <li class="header-item">
                                    <a class="header-link" href="#">pages</a>                            
                                </li>
                                <li class="header-item">
                                    <a class="header-link" href="#">category</a>
                                </li>
                                <li class="header-item">
                                    <a class="header-link" href="#">layouts</a>                                                      
                                </li>
                                <li class="header-item">
                                    <a class="header-link" href="#">about</a>                            
                                </li>
                                <li class="header-item">
                                    <a class="header-link" href="#">features</a>
                                </li>  
                            </div>                            
                        </div>                                              
                    </div> 
                    <div class="header-social-media">
                        <ul class="head-media-list">
                            <li class="mid-social-item">
                                <a class="social-link" href="">
                                    <i class="fas fa-share-alt"></i>
                                </a>
                            </li>
                            <li class="mid-social-item">
                                <a class="social-link" href="">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </li>
                            <li class="mid-social-item">
                                <a class="social-link" href="">
                                    <i class="fab fa-youtube"></i>
                                </a>
                            </li>
                            <li class="mid-social-item">
                                <a class="social-link" class="social-link" href="">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                            <li class="mid-social-item">
                                <a class="social-link" class="social-link" href="">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
                        </ul>
                    </div>                  
                </div> 
                <div id="main">                    
                    <span class="nav-button" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776;</span>
                    <div class="mid-banner-content wow slideInDown" data-wow-duration="1s" data-wow-delay="0s">
                        <div class="mid-categ">
                            <span class="" role="button">Mountaineering</span>
                        </div>
                        <div class="mid-heads">
                            <h1>Look Deep Into My Bikini and Then You’ll Understand Everything Better</h1>
                            <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor 
                                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                    exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </span>
                        </div>
                        <div class="mid-social-media">
                            <ul class="mid-link">
                                <li class="mid-social-item">
                                    <a class="social-link" href="">
                                        <i class="fas fa-share-alt"></i>
                                    </a>
                                </li>
                                <li class="mid-social-item">
                                    <a class="social-link" href="">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </li>
                                <li class="mid-social-item">
                                    <a class="social-link" href="">
                                        <i class="fab fa-youtube"></i>
                                    </a>
                                </li>
                                <li class="mid-social-item">
                                    <a class="social-link" class="social-link" href="">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </li>
                                <li class="mid-social-item">
                                    <a class="social-link" class="social-link" href="">
                                        <i class="fab fa-instagram"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>                
            </div>            
        </div>
    </section>

    <!--banner ends-->


    <!--feature blog section starts-->
    <?php include("featured.php"); ?>
    <!--feature blog section ends-->

    <!-- blog contetn starts-->

    <div class="blog-content-wrapper home-page">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-8 col-12">
                    <div class="blog-list wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                        <h6 class="next-head">blog-list</h6>
                        <div class="card mid-card">
                            <div class="blog-card-head">
                                <img class="blog-start-img" src="images/travel-essentials.jpg" alt="">
                                <div class="card-first-btn">
                                    <a href="#" class="display-list-blog ">Travel</a>
                                </div>                                                                    
                                <div class="card-first-mid-btn">
                                    <a href="#" class="btn after-btn">
                                        <span class="angle-button">Read more</span>
                                        <img class="angle-img" src="images/arrow-right.png" alt="">
                                    </a>                                                                     
                                </div>                              
                            </div>                                                                                 
                            <div class="card-body">
                                <div class="like-button-wrap">
                                    <div class="like-button">
                                        <div class="read-more-like">
                                            <div class="btn-like-wrap">
                                                <div class="like-back">
                                                    <i class="fas fa-heart"></i>
                                                </div> 
                                            </div>  
                                        </div>                                                                           
                                    </div>
                                </div>
                                <h5 class="card-title">The Beauty Of Sunset Create A Romantic Magic In The Hearts Of 
                                  Lovers and a Cute </h5>
                                 <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                   sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut 
                                   enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                                    aliquip ex ea commodo consequat. </p>    
                                <div class="theme-by">
                                    <span>By Spiderbuzz &nbsp  &nbsp  |  &nbsp &nbsp    July 29, 2019</span>
                                </div>
                                <div class="blog-list-media">
                                    <ul class="mid-link">
                                        <li class="mid-social-item">
                                            <a class="social-link" href="">
                                                <i class="fab fa-facebook-f"></i>
                                            </a>
                                        </li>
                                        <li class="mid-social-item">
                                            <a class="social-link" href="">
                                                <i class="fab fa-youtube"></i>
                                            </a>
                                        </li>
                                        <li class="mid-social-item">
                                            <a class="social-link" class="social-link" href="">
                                                <i class="fab fa-twitter"></i>
                                            </a>
                                        </li>
                                        <li class="mid-social-item">
                                            <a class="social-link" class="social-link" href="">
                                                <i class="fab fa-instagram"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>                        
                            </div>
                          </div>
                    </div>
                    <div class="blog-list-firstad wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                        <img src="images/landscape-banner-ad.jpg" alt="">
                    </div>
                    <div class="blog-list-sec wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">                       
                        <div class="card mid-card">
                            <div class="blog-card-head">
                            <img class="blog-start-img" src="images/travel-essentials.jpg" alt="">
                                <div class="card-first-btn">
                                    <a href="#" class="display-list-blog ">Travel</a>
                                </div>                                                                    
                                <div class="card-first-mid-btn">
                                    <div class="read-more-btn">
                                        <a href="#" class="btn after-btn">
                                            <span class="angle-button">Read more</span>
                                            <img class="angle-img" src="images/arrow-right.png" alt="">
                                        </a>
                                    </div>                                                                 
                                </div>                              
                            </div>                                                                                  
                            <div class="card-body">
                                <div class="like-button-wrap">
                                    <div class="like-button">
                                        <div class="read-more-like">
                                            <div class="btn-like-wrap">
                                                <div class="like-back">
                                                    <i class="fas fa-heart"></i>
                                                </div> 
                                            </div>  
                                        </div>                                                                           
                                    </div>
                                </div>
                                <h5 class="card-title">The Beauty Of Sunset Create A Romantic Magic In The Hearts Of 
                                    Lovers and a Cute  </h5>
                                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut 
                                    enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                                    aliquip ex ea commodo consequat. </p>    
                                <div class="theme-by">
                                    <span>By Spiderbuzz &nbsp  &nbsp  |  &nbsp &nbsp    July 29, 2019</span>
                                </div>
                                <div class="blog-list-media">
                                    <ul class="mid-link">
                                        <li class="mid-social-item inherit-link">
                                            <a class="social-link" href="">
                                                <i class="fab fa-facebook-f"></i>
                                            </a>
                                        </li>
                                        <li class="mid-social-item inherit-link">
                                            <a class="social-link" href="">
                                                <i class="fab fa-youtube"></i>
                                            </a>
                                        </li>
                                        <li class="mid-social-item inherit-link">
                                            <a class="social-link" class="social-link" href="">
                                                <i class="fab fa-twitter"></i>
                                            </a>
                                        </li>
                                        <li class="mid-social-item inherit-link">
                                            <a class="social-link" class="social-link" href="">
                                                <i class="fab fa-instagram"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>                        
                            </div>
                        </div>
                    </div>
                    <div class="about-blog-section wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                        <div class="about-blog-wrapper">
                            <div class="blog-wrap-color">
                                <div class="row">
                                    <div class="col-xl-6 col-lg-6 col-md-12 col-12 blog-wrap-col">
                                        <div class="about-img">
                                            <img src="images/gymlady.jpg" alt="">                                      
                                            <div class="like-button-list">
                                                <div class="like-button">
                                                    <div class="read-more-like">
                                                        <div class="btn-like-wrap">
                                                            <div class="like-back">
                                                                <i class="fas fa-heart"></i>
                                                            </div> 
                                                        </div>  
                                                    </div>                                                                           
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-6 col-md-12 col-12">
                                        <div class="beauty-head">
                                            <span>By Spiderbuzz &nbsp &nbsp    |  &nbsp  &nbsp  July 29, 2019</span>
                                            <h1>The Beauty Of Sunset Create A Romantic Magic In The Hearts Of
                                                Lovers and a Cute</h1>
                                            <div class="like-comment next-top">
                                                <ul class="like-link">
                                                    <li class="like-item">
                                                        <i class="fas fa-heart"></i>
                                                        <span>568</span>
                                                        <a>Likes</a>
                                                    </li>
                                                    <li class="like-item">
                                                        <i class="fas fa-comments"></i>
                                                        <span>268</span>
                                                        <a>Comments</a>
                                                    </li>
                                                    <li class="like-item">
                                                        <i class="fas fa-share"></i>
                                                        <span>10</span>
                                                        <a>Shares</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <p class="display-none-para">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed 
                                                do eiusmod tempor</p>                                       
                                            <div class="read-more-btn">
                                                <a href="#" class="btn after-btn">
                                                    <span class="angle-button">Read more</span>
                                                    <img class="angle-img" src="images/arrow-right.png" alt="">
                                                </a>
                                            </div>                                          
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="blog-wrap-color mt-4">
                                <div class="row">
                                    <div class="col-xl-6 col-lg-6 col-md-12 col-12 blog-wrap-col">
                                        <div class="about-img">
                                            <img src="images/gymlady.jpg" alt="">                                      
                                            <div class="like-button-list">
                                                <div class="like-button">
                                                    <div class="read-more-like">
                                                        <div class="btn-like-wrap">
                                                            <div class="like-back">
                                                                <i class="fas fa-heart"></i>
                                                            </div> 
                                                        </div>  
                                                    </div>                                                                           
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-6 col-md-12 col-12">
                                        <div class="beauty-head">
                                            <span>By Spiderbuzz &nbsp &nbsp    |  &nbsp  &nbsp  July 29, 2019</span>
                                            <h1>The Beauty Of Sunset Create A Romantic Magic In The Hearts Of
                                                Lovers and a Cute</h1>
                                            <div class="like-comment next-top">
                                                <ul class="like-link">
                                                    <li class="like-item">
                                                        <i class="fas fa-heart"></i>
                                                        <span>568</span>
                                                        <a>Likes</a>
                                                    </li>
                                                    <li class="like-item">
                                                        <i class="fas fa-comments"></i>
                                                        <span>268</span>
                                                        <a>Comments</a>
                                                    </li>
                                                    <li class="like-item">
                                                        <i class="fas fa-share"></i>
                                                        <span>10</span>
                                                        <a>Shares</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <p class="display-none-para">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed 
                                                do eiusmod tempor</p>                                       
                                            <div class="read-more-btn">
                                                <a href="#" class="btn after-btn">
                                                    <span class="angle-button">Read more</span>
                                                    <img class="angle-img" src="images/arrow-right.png" alt="">
                                                </a>
                                            </div>                                          
                                        </div>
                                    </div>
                                </div>
                            </div>        
                        </div>
                    </div>
                    <div class="about-blog-section wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                        <div class="about-blog-wrapper">
                            <div class="container">
                                <div class="row">
                                    
                                </div>
                            </div>            
                        </div>
                    </div>
                    <div class="horizontal-blog-wrapper">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-12 about">
                                <div class="card wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                                    <div class="card-header-sec">
                                        <img class="two-section-blog" src="images/food2.jpg" alt="">
                                        <div class="read-more-btn">
                                            <a href="#" class="btn after-btn">
                                                <span class="angle-button">Read more</span>
                                                <img class="angle-img" src="images/arrow-right.png" alt="">
                                            </a>
                                        </div>                                            
                                    </div>                                    
                                    <div class="card-body about-title">
                                        <span class="card-span">By Spiderbuzz &nbsp &nbsp    |  &nbsp  &nbsp  July 29, 2019</span>
                                        <h5 class="card-title">The Beauty Of Sunset Create A Romantic Magic In The Hearts of</h5>
                                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                        <div class="like-button-hori">
                                            <div class="like-back">
                                                <div class="icon-wrap">
                                                    <i class="fas fa-heart"></i>
                                                </div>
                                            </div>                                               
                                        </div>                                     
                                    </div>
                                </div>                                 
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-12 about-hori">
                                <div class="card wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                                    <div class="card-header-sec">
                                        <img class="two-section-blog" src="images/food2.jpg" alt="">
                                        <div class="read-more-btn">
                                            <a href="#" class="btn after-btn">
                                                <span class="angle-button">Read more</span>
                                                <img class="angle-img" src="images/arrow-right.png" alt="">
                                            </a>
                                        </div>                                            
                                    </div>                                    
                                    <div class="card-body about-title ">
                                        <span class="card-span">By Spiderbuzz &nbsp &nbsp    |  &nbsp  &nbsp  July 29, 2019</span>
                                        <h5 class="card-title">The Beauty Of Sunset Create A Romantic Magic In The Hearts of</h5>
                                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut</p>
                                        <div class="like-button-hori">
                                            <div class="like-back">
                                                <div class="icon-wrap">
                                                    <i class="fas fa-heart"></i>
                                                </div>
                                            </div>                                              
                                        </div>                                     
                                    </div>
                                </div>                                 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-12">
                    <div class="next-column wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                        <div class="blog-search-list">
                            <h6 class="next-head">search</h6>
                            <div class="search-box">
                                <form class="form-inline">
                                    <input class="col-xl-7 col-lg-7 col-md-12 col-12 form-control" type="search" placeholder="Search for blog" aria-label="Search">
                                    <div class="btn-wrapper-search col-xl-5 col-lg-5 col-md-6 col-12">
                                        <button class="btn" type="submit">Search</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="about-author wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                            <h1 class="header-author">about author</h1>
                            <div class="author-image">
                                <img src="images/author.jpg" alt="">
                            </div>
                            <div class="author-name">
                                <span class="author-name-tag">Kierrra Astor Doe</span>
                                <p class="author-name-para">Lorem ipsum dolor sit amet, consectetur adipiscing 
                                elit, sed do eiusmod tempor incididunt aduhasvd</p>
                                <div class="read-more-btn">
                                    <a href="#" class="btn after-btn">
                                        <span class="angle-button">Read more</span>
                                        <img class="angle-img" src="images/arrow-right.png" alt="">
                                    </a>
                                </div>   
                            </div>                            
                        </div>
                        <div class="next-col-ad wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                            <img src="images/banner-ad.jpg" alt="">
                        </div>
                        <div class="about-news wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                            <h1 class="news-letter">newsletter</h1>
                            <div class="suscribe-newsletter">
                                <div class="news-media">
                                    <ul class="news-item">
                                        <li class="news-link">
                                            <a href=""><i class="fab fa-facebook-f"></i></a>
                                        </li>
                                        <li class="news-link">
                                            <a href=""><i class="fab fa-youtube"></i></a>
                                        </li>
                                        <li class="news-link">
                                            <a href=""><i class="fab fa-twitter"></i></a>
                                        </li>
                                        <li class="news-link">
                                            <a href=""><i class="fab fa-instagram"></i></a>
                                        </li>
                                    </ul>                                        
                                </div>
                                <div class="news-head">
                                    <h1 class="news-header">Subscribe our newsletter and we’ll get back to you</h1>
                                </div>
                                <div class="news-form">
                                    <form class="form-inline">
                                        <div class="form-group col-12">                                            
                                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Your email address.....">
                                        </div>
                                        <div class="read-more-btn">
                                            <a href="#" class="btn after-btn">
                                                <span class="angle-button">Read more</span>
                                                <img class="angle-img" src="images/arrow-right.png" alt="">
                                            </a>
                                        </div>                                             
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="recent-posts wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                            <h1 class="recent-post-line">recent posts</h1>
                            <div class="about-recent-post">
                                <div class="recent-post-click">
                                    <div class="recent-image">
                                        <img src="images/yoga-lady.jpg" alt="">
                                    </div>
                                    <div class="about-recent-head">
                                        <div class="recent-wrap">
                                            <h1 class="recent-head">The Beauty Of Sunset Create A Romantic</h1>
                                            <div class="read-more-btn">
                                                <a href="#" class="btn after-btn">
                                                    <span class="angle-button">Read more</span>
                                                    <img class="angle-img" src="images/arrow-right.png" alt="">
                                                </a>
                                            </div> 
                                        </div>
                                    </div>                                        
                                </div>
                                <div class="recent-post-click">
                                    <div class="recent-image">
                                        <img src="images/yoga-lady.jpg" alt="">
                                    </div>
                                    <div class="about-recent-head">
                                        <div class="recent-wrap in-it">
                                            <span class="recent-line">July 29, 2019</span>
                                            <h1 class="recent-head">The Beauty Of Sunset Create A Romantic</h1>
                                        </div>
                                    </div>                                       
                                </div>
                                <div class="recent-post-click">
                                    <div class="recent-image">
                                        <img src="images/yoga-lady.jpg" alt="">
                                    </div>
                                    <div class="about-recent-head">
                                        <div class="recent-wrap in-it">
                                            <span class="recent-line">July 29, 2019</span>
                                            <h1 class="recent-head">The Beauty Of Sunset Create A Romantic</h1>
                                        </div>
                                    </div>                                        
                                </div>
                            </div>
                        </div>
                        <div class="post-tags wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                            <h1 class="post-line">tags</h1>
                            <div class="tag-buttons">
                                <div class="tag-button-click">
                                    <a class="btn" href="#" role="button">bikini</a>
                                </div>
                                <div class="tag-button-click">
                                    <a class="btn" href="#" role="button">travel</a>
                                </div>
                                <div class="tag-button-click">
                                    <a class="btn" href="#" role="button">adventure</a>
                                </div>
                                <div class="tag-button-click">
                                    <a class="btn" href="#" role="button">vacation</a>
                                </div>
                            </div>
                        </div>                     
                    </div>                    
                </div>
            </div>
        </div>
    </div>

    <!--blog contetn section ends-->

    <!--blog two section starts-->

    <div class="blog-two-wrapper wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
        <div class="container">
            <div class="two-blog-top">
                <div class="row">
                    <div class="col-xl-5 col-lg-5 col-md-5 col-12">
                        <div class="image-two">
                            <div class="image-two-wrapper">
                                <img src="images/girl.jpg" alt="">
                                <div class="button-wrapper">
                                    <div class="like-back">
                                        <div class="icon-wrap">
                                            <i class="fas fa-heart"></i>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-7 col-md-7 col-12">
                        <div class="two-content">
                            <div class="beauty-head">
                                <span class="top-span">By Spiderbuzz &nbsp &nbsp    |  &nbsp  &nbsp  July 29, 2019</span>
                                <h1>The Beauty Of Sunset Create A Romantic Magic In The Hearts Of Lovers and a Cute Baby In Future</h1>
                                <div class="like-comment">
                                    <ul class="like-link">
                                        <li class="like-item">
                                            <i class="fas fa-heart"></i>
                                            <span>568</span>
                                            <a>Likes</a>
                                        </li>
                                        <li class="like-item">
                                            <i class="fas fa-comments"></i>
                                            <span>268</span>
                                            <a>Comments</a>
                                        </li>
                                        <li class="like-item">
                                            <i class="fas fa-share"></i>
                                            <span>10</span>
                                            <a>Shares</a>
                                        </li>
                                    </ul>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
                                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo 
                                    consequat. </p>
                                <div class="button-social-media">
                                    <div class="read-more-btn">
                                        <a href="#" class="btn after-btn">
                                            <span class="angle-button">Read more</span>
                                            <img class="angle-img" src="images/arrow-right.png" alt="">
                                        </a>
                                    </div>
                                    <div class="news-media">
                                        <ul class="news-item">
                                            <li class="news-link">
                                                <a href=""><i class="fab fa-facebook-f"></i></a>
                                            </li>
                                            <li class="news-link">
                                                <a href=""><i class="fab fa-youtube"></i></a>
                                            </li>
                                            <li class="news-link">
                                                <a href=""><i class="fab fa-twitter"></i></a>
                                            </li>
                                            <li class="news-link">
                                                <a href=""><i class="fab fa-instagram"></i></a>
                                            </li>
                                        </ul>                                        
                                    </div>  
                                </div>                                                                          
                            </div>
                        </div>
                    </div>               
                </div>
            </div>
            <div class="two-blog-top">
                <div class="row">
                    <div class="col-xl-5 col-lg-5 col-md-5 col-12">
                        <div class="image-two">
                            <div class="image-two-wrapper">
                                <img src="images/girl.jpg" alt="">
                                <div class="button-wrapper">
                                    <div class="like-back">
                                        <div class="icon-wrap">
                                            <i class="fas fa-heart"></i>
                                        </div>
                                    </div>
                                </div> 
                            </div>                                               
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-7 col-md-7 col-12">
                        <div class="two-content">
                            <div class="beauty-head">
                                <span class="top-span">By Spiderbuzz &nbsp &nbsp    |  &nbsp  &nbsp  July 29, 2019</span>
                                <h1>The Beauty Of Sunset Create A Romantic Magic In The Hearts Of Lovers and a Cute Baby In Future</h1>
                                <div class="like-comment">
                                    <ul class="like-link">
                                        <li class="like-item">
                                            <i class="fas fa-heart"></i>
                                            <span>568</span>
                                            <a>Likes</a>
                                        </li>
                                        <li class="like-item">
                                            <i class="fas fa-comments"></i>
                                            <span>268</span>
                                            <a>Comments</a>
                                        </li>
                                        <li class="like-item">
                                            <i class="fas fa-share"></i>
                                            <span>10</span>
                                            <a>Shares</a>
                                        </li>
                                    </ul>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
                                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo 
                                    consequat. </p>
                                <div class="button-social-media">
                                    <div class="read-more-btn">
                                        <a href="#" class="btn after-btn">
                                            <span class="angle-button">Read more</span>
                                            <img class="angle-img" src="images/arrow-right.png" alt="">
                                        </a>
                                    </div>
                                    <div class="news-media">
                                        <ul class="news-item">
                                            <li class="news-link">
                                                <a href=""><i class="fab fa-facebook-f"></i></a>
                                            </li>
                                            <li class="news-link">
                                                <a href=""><i class="fab fa-youtube"></i></a>
                                            </li>
                                            <li class="news-link">
                                                <a href=""><i class="fab fa-twitter"></i></a>
                                            </li>
                                            <li class="news-link">
                                                <a href=""><i class="fab fa-instagram"></i></a>
                                            </li>
                                        </ul>                                        
                                    </div>  
                                </div>                                                                          
                            </div>
                        </div>
                    </div>               
                </div>
            </div>            
        </div>
    </div>

    <!--blog two section ends-->

    <!--blog-list-views-->

    <section class="blog-view-list wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
        <div class="blog-view-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-xl-8 col-lg-8 col-md-8 col-12">
                        <h1 class="list-view-head">blog list view</h1>
                        <div class="about-image">
                            <div class="row">
                                <div class="col-xl-6 col-lg-6 col-md-12 col-12">
                                    <div class="about-img">   
                                        <img src="images/girls.jpg" alt="">                                         
                                        <div class="like-button-blog">
                                            <div class="like-back">
                                                <div class="icon-wrap">
                                                    <i class="fas fa-heart"></i>
                                                </div>
                                            </div>                                             
                                        </div> 
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-12 col-12">
                                    <div class="beauty-head">
                                        <div class="share-icons">
                                            <ul class="share-items">
                                                <li class="share-link">
                                                    <a href="">
                                                        <i class="fas fa-heart"></i>
                                                    </a>                                           
                                                </li>
                                                <li class="share-link">
                                                    <a href="">
                                                        <i class="fas fa-comments"></i>     
                                                    </a>                                                                              
                                                </li>
                                                <li class="share-link">
                                                    <a href="">
                                                        <i class="fas fa-share"></i>
                                                    </a>                                                                                        
                                                </li>
                                            </ul>
                                        </div>
                                        <span>By Spiderbuzz &nbsp &nbsp    |  &nbsp  &nbsp  July 29, 2019</span>
                                        <h1>The Beauty Of Sunset Create A Romantic Magic In The Hearts 
                                            </h1>
                                        <div class="like-comment">
                                            <ul class="like-link">
                                                <li class="like-item">
                                                    <i class="fas fa-heart"></i>
                                                    <span>568</span>
                                                    <a>Likes</a>
                                                </li>
                                                <li class="like-item">
                                                    <i class="fas fa-comments"></i>
                                                    <span>268</span>
                                                    <a>Comments</a>
                                                </li>
                                                <li class="like-item">
                                                    <i class="fas fa-share"></i>
                                                    <span>10</span>
                                                    <a>Shares</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed 
                                            do eiusmod tempor</p>                                       
                                        <div class="read-more-btn">
                                            <a href="#" class="btn after-btn">
                                                <span class="angle-button">Read more</span>
                                                <img class="angle-img" src="images/arrow-right.png" alt="">
                                            </a>
                                        </div>                                          
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-12">
                        <div class="recent-posts">
                            <h1 class="recent-post-line">blog sidebar</h1>
                            <div class="about-recent-post">
                                <div class="recent-post-click">
                                    <div class="recent-image">
                                        <img src="images/yoga-lady.jpg" alt="">
                                    </div>
                                    <div class="about-recent-head">
                                        <div class="recent-wrap">
                                            <h1 class="recent-head">The Beauty Of Sunset Create A Romantic</h1>
                                            <div class="read-more-btn">
                                                <a href="#" class="btn after-btn">
                                                    <span class="angle-button">Read more</span>
                                                    <img class="angle-img" src="images/arrow-right.png" alt="">
                                                </a>
                                            </div> 
                                        </div>
                                    </div>                                        
                                </div>
                                <div class="recent-post-click">
                                    <div class="recent-image">
                                        <img src="images/yoga-lady.jpg" alt="">
                                    </div>
                                    <div class="about-recent-head">
                                        <div class="recent-wrap in-it">
                                            <span class="recent-line">July 29, 2019</span>
                                            <h1 class="recent-head">The Beauty Of Sunset Create A Romantic</h1>
                                        </div>
                                    </div>                                       
                                </div>
                                <div class="recent-post-click">
                                    <div class="recent-image">
                                        <img src="images/yoga-lady.jpg" alt="">
                                    </div>
                                    <div class="about-recent-head">
                                        <div class="recent-wrap in-it">
                                            <span class="recent-line">July 29, 2019</span>
                                            <h1 class="recent-head">The Beauty Of Sunset Create A Romantic</h1>
                                        </div>
                                    </div>                                        
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
    </section>

    <!--blog-list-views-->

    <!--follow us on instagram-->
   
    <div class="insta-follow-wrapper wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
        <div class="container">
            <h1 class="follow-instagram">follow us on instagram</h1>
            <div class="insta-photo-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-3 col-lg-3 col-md-3 col-6 pad-img">
                            <div class="insta-pic">
                                <img src="images/family.jpg" alt="">
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3 col-6 pad-img">
                            <div class="insta-pic">
                                <img src="images/dog.jpg" alt="">
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3 col-6 pad-img">
                            <div class="insta-pic">
                                <img src="images/food.jpg" alt="">
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3 col-6 pad-img">
                            <div class="insta-pic">
                                <img src="images/curly-hari.jpg" alt="">
                            </div>
                        </div>                           
                    </div>
                </div>
            </div>
            <div class="wrapper">
                <div class="insta-button">
                    <div class="read-more-btn">
                        <a href="#" class="btn after-btn">
                            <span class="angle-button">Read more</span>
                            <img class="angle-img" src="images/arrow-right.png" alt="">
                        </a>
                    </div>    
                </div>                                    
            </div> 
        </div>       
    </div>

    <!--follow us on instagram-->

    <!--footer-sec-->

   <div class="footer-section-wrapper wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
       <div class="container">
           <div class="row">
               <div class="col-lg-3 col-md-4 col-sm-4 col-12">
                   <div class="blog88-header">
                       <h1 class="blog88-head">blog88</h1>
                       <ul class="footer-blog">
                           <li class="footer-link">
                               <a href="" class="footer-address" href="">23 King Street, 5th Avenue, New York<br>
                                    1-2355-3345-5
                                </a>
                           </li>
                           <li class="footer-link">
                                <a href="" class="footer-call" href="">call@blog88</a>
                           </li>
                       </ul>
                   </div>                                                  
               </div>
               <div class="col-lg-3 col-md-4 col-sm-4 col-12">
                   <div class="foot-recent-wrap">
                        <h1 class="blog88-head">recent posts</h1>
                        <div class="recent-foot-post">
                            <ul class="footer-blog recent-foot">
                                <li class="footer-link">
                                    <a href="">Food</a>
                                </li>
                                <li class="footer-link">
                                    <a href="">Travel</a>
                                </li>
                                <li class="footer-link">
                                    <a href="">Fashion</a>
                                </li>
                                <li class="footer-link">
                                    <a href="">Trending</a>
                                </li>
                                <li class="footer-link">
                                    <a href="" >Photography</a>
                                </li>
                                <li class="footer-link">
                                    <a href="" >Technology</a>
                                </li>
                            </ul>
                        </div>
                   </div>
               </div>
               <div class="col-lg-3 col-md-4 col-sm-4 col-12">
                   <div class="footer-media">
                        <h1 class="blog88-head">follow us on </h1>
                        <div class="recent-foot-post">                           
                            <ul class="share-items">
                                <div class="li-wrapper">
                                    <li class="share-link face-book">
                                        <a href="">
                                            <i class="fab fa-facebook-f"></i>
                                        </a>                                           
                                    </li>
                                </div>
                                <div class="li-wrapper">
                                    <li class="share-link face-book">
                                        <a href="">
                                            <i class="fab fa-youtube"></i> 
                                        </a>                                           
                                    </li>
                                </div>
                                <div class="li-wrapper">
                                    <li class="share-link face-book">
                                        <a href="">
                                            <i class="fab fa-twitter twi-tter"></i>
                                        </a>                                           
                                    </li>
                                </div>
                                <div class="li-wrapper">
                                    <li class="share-link face-book">
                                        <a href="">
                                            <i class="fab fa-instagram"></i>
                                        </a>                                           
                                    </li>
                                </div>
                            </ul>
                        </div>
                   </div>                    
                </div> 
                <div class="col-lg-3 col-md-4 col-sm-4 col-12 news-foot">
                    <div class="footer-top-wrapper">
                        <h1 class="blog88-head">news letter</h1>
                        <div class="footer-form">
                            <div class="news-form">
                                <form class="form-inline">
                                    <div class="form-group col-12">                                          
                                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Your email address.....">
                                    </div>
                                    <div class="read-more-btn">
                                        <a href="#" class="btn after-btn">
                                            <span class="angle-button">Suscribe</span>
                                            <img class="angle-img" src="images/arrow-right.png" alt="">
                                        </a>
                                    </div>                                            
                                </form>
                            </div>
                        </div>
                    </div>
                </div> 
           </div>           
       </div>
   </div>

    <!--footer-sec-->
  
     <!--footer-bottom-sec-->
    
    <div class="bottom-foot-wrapper">
        <div class="container">
            <div class="row wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                <div class="col-xl-8 col-lg-8 col-md-7 col-12">
                    <div class="footer-link">
                        <ul class="bottom-link">
                            <li class="footer-items">
                                <a href="" class="bottom-tag">FAQS</a>
                            </li>
                            <li class="footer-items">
                                <a href="" class="bottom-tag">Privacy Policies</a>
                            </li>
                            <li class="footer-items">
                                <a href="" class="bottom-tag">Terms and Conditions</a>
                            </li>
                            <li class="footer-items">
                                <a href="" class="bottom-tag">About</a>
                            </li>
                            <li class="footer-items">
                                <a href="" class="bottom-tag">Careers</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-5 col-12">
                    <div class="footer-last">
                        <span class="span-last"><i class="far fa-copyright"></i> 2014 Blogger Life. Design by Themewing</span>
                    </div>
                </div>
            </div>
        </div>    
    </div>    

    <!--footer-bottom-sec-->

    <!-- Java JavaScript -->
    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/bootstrap.bundle.min.js" ></script>
    <script src="js/wow.min.js"></script>
    <script src="js/app.js"></script>

    <script>
        function openNav() {
            document.getElementById("mySidenav").style.width = "400px";
            document.getElementById("main").style.marginLeft = "400px";
        }
        
        function closeNav() {
            document.getElementById("mySidenav").style.width = "0";
            document.getElementById("main").style.marginLeft= "0";
        }
    </script>
</body>
</html>