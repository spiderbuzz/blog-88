<!-- banner -->

<div id="carouselExampleCaptions" class="carousel slide layout-five carousel-second-page" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
        <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="images/firework.jpg" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block">
                <div class="animated zoomIn mid-banner-content">
                    <div class="mid-categ">
                        <span class="" role="button">Mountaineering</span>
                    </div>
                    <div class="mid-heads">
                        <h1>Look Deep Into My Bikini and Then You’ll Understand Everything Better</h1>
                        <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor 
                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                exercitation </span>
                    </div>
                    <div class="mid-social-media">
                        <ul class="mid-link">
                            <li class="mid-social-item">
                                <a class="social-link" href="">
                                    <i class="fas fa-share-alt"></i>
                                </a>
                            </li>
                            <li class="mid-social-item">
                                <a class="social-link" href="">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </li>
                            <li class="mid-social-item">
                                <a class="social-link" href="">
                                    <i class="fab fa-youtube"></i>
                                </a>
                            </li>
                            <li class="mid-social-item">
                                <a class="social-link" href="">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                            <li class="mid-social-item">
                                <a class="social-link" href="">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <img src="images/firework.jpg" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block">
            <div class="animated zoomIn mid-banner-content">
                    <div class="mid-categ">
                        <span class="" role="button">Mountaineering</span>
                    </div>
                    <div class="mid-heads">
                        <h1>Look Deep Into My Bikini and Then You’ll Understand Everything Better</h1>
                        <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor 
                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                exercitation</span>
                    </div>
                    <div class="mid-social-media">
                        <ul class="mid-link">
                            <li class="mid-social-item">
                                <a class="social-link" href="">
                                    <i class="fas fa-share-alt"></i>
                                </a>
                            </li>
                            <li class="mid-social-item">
                                <a class="social-link" href="">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </li>
                            <li class="mid-social-item">
                                <a class="social-link" href="">
                                    <i class="fab fa-youtube"></i>
                                </a>
                            </li>
                            <li class="mid-social-item">
                                <a class="social-link" href="">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                            <li class="mid-social-item">
                                <a class="social-link" href="">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <img src="images/firework.jpg" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block">
            <div class="animated zoomIn mid-banner-content">
                    <div class="mid-categ">
                        <span class="" role="button">Mountaineering</span>
                    </div>
                    <div class="mid-heads">
                        <h1>Look Deep Into My Bikini and Then You’ll Understand Everything Better</h1>
                        <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor 
                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                exercitation</span>
                    </div>
                    <div class="mid-social-media">
                        <ul class="mid-link">
                            <li class="mid-social-item">
                                <a class="social-link" href="">
                                    <i class="fas fa-share-alt"></i>
                                </a>
                            </li>
                            <li class="mid-social-item">
                                <a class="social-link" href="">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </li>
                            <li class="mid-social-item">
                                <a class="social-link" href="">
                                    <i class="fab fa-youtube"></i>
                                </a>
                            </li>
                            <li class="mid-social-item">
                                <a class="social-link" href="">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                            <li class="mid-social-item">
                                <a class="social-link" href="">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="carousel-controllers">
        <a class="animated zoomIn carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
            <div class="carousel-control">
                <i class="fas fa-angle-left"></i>
            </div>
        </a>
        <a class="animated zoomIn carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
            <div class="carousel-control">
                <i class="fas fa-angle-right"></i>
            </div>
        </a>
    </div>
    <div class="mid-category homepage-two-category">
        <div class="container blog-list-overlay">
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-6 col-12">
                    <div class="overlay-wrap">
                        <img src="images/balloon.jpg" alt="">
                        <div class="blog-list-header">
                            <span class="d-inline-block">travel</span>
                            <a href="" class="d-block">Look Deep Into My Bikini and Then You’ll Understand</a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-12 mobile-view">
                    <div class="overlay-wrap">
                        <img src="images/gaming.jpg" alt="">
                        <div class="blog-list-header">
                            <span class="d-inline-block">gaming</span>
                            <a href="" class="d-block">Look Deep Into My Bikini and Then You’ll Understand</a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-12 tablet-view">
                    <div class="overlay-wrap">
                        <img src="images/fruits.jpg" alt="">
                        <div class="blog-list-header">
                            <span class="d-inline-block">fruits</span>
                            <a href="" class="d-block">Look Deep Into My Bikini and Then You’ll Understand</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- banner -->