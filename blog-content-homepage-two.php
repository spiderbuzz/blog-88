<div class="blog-content-wrapper home-page-two">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 col-lg-8 col-md-8 col-12">
                <div class="blog-list wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                    <h6 class="next-head">blog-list</h6>
                    <div class="card mid-card">
                        <div class="blog-card-head">
                            <img class="blog-start-img" src="images/travel-essentials.jpg" alt="">
                            <div class="card-first-btn">
                                <a href="#" class="display-list-blog ">Travel</a>
                            </div>                                                                    
                            <div class="card-first-mid-btn">
                                <a href="#" class="btn after-btn">
                                    <span class="angle-button">Read more</span>
                                    <img class="angle-img" src="images/arrow-right.png" alt="">
                                </a>                                                                     
                            </div>                              
                        </div>                                                                                 
                        <div class="card-body">
                            <div class="like-button-wrap">
                                <div class="like-button">
                                    <div class="read-more-like">
                                        <div class="btn-like-wrap">
                                            <div class="like-back">
                                                <i class="fas fa-heart"></i>
                                            </div> 
                                        </div>  
                                    </div>                                                                           
                                </div>
                            </div>
                            <h5 class="card-title">The Beauty Of Sunset Create A Romantic Magic In The Hearts Of 
                                Lovers and a Cute </h5>
                                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut 
                                enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                                aliquip ex ea commodo consequat. </p>    
                            <div class="theme-by">
                                <span>By Spiderbuzz &nbsp  &nbsp  |  &nbsp &nbsp    July 29, 2019</span>
                            </div>
                            <div class="blog-list-media">
                                <ul class="mid-link">
                                    <li class="mid-social-item">
                                        <a class="social-link" href="">
                                            <i class="fab fa-facebook-f"></i>
                                        </a>
                                    </li>
                                    <li class="mid-social-item">
                                        <a class="social-link" href="">
                                            <i class="fab fa-youtube"></i>
                                        </a>
                                    </li>
                                    <li class="mid-social-item">
                                        <a class="social-link" class="social-link" href="">
                                            <i class="fab fa-twitter"></i>
                                        </a>
                                    </li>
                                    <li class="mid-social-item">
                                        <a class="social-link" class="social-link" href="">
                                            <i class="fab fa-instagram"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>                        
                        </div>
                        </div>
                </div>
                <div class="blog-list-firstad wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                    <img src="images/landscape-banner-ad.jpg" alt="">
                </div>
                <div class="blog-list-sec wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">                       
                    <div class="card mid-card">
                        <div class="blog-card-head">
                        <img class="blog-start-img" src="images/travel-essentials.jpg" alt="">
                            <div class="card-first-btn">
                                <a href="#" class="display-list-blog ">Travel</a>
                            </div>                                                                    
                            <div class="card-first-mid-btn">
                                <div class="read-more-btn">
                                    <a href="#" class="btn after-btn">
                                        <span class="angle-button">Read more</span>
                                        <img class="angle-img" src="images/arrow-right.png" alt="">
                                    </a>
                                </div>                                                                 
                            </div>                              
                        </div>                                                                                  
                        <div class="card-body">
                            <div class="like-button-wrap">
                                <div class="like-button">
                                    <div class="read-more-like">
                                        <div class="btn-like-wrap">
                                            <div class="like-back">
                                                <i class="fas fa-heart"></i>
                                            </div> 
                                        </div>  
                                    </div>                                                                           
                                </div>
                            </div>
                            <h5 class="card-title">The Beauty Of Sunset Create A Romantic Magic In The Hearts Of 
                                Lovers and a Cute  </h5>
                            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut 
                                enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                                aliquip ex ea commodo consequat. </p>    
                            <div class="theme-by">
                                <span>By Spiderbuzz &nbsp  &nbsp  |  &nbsp &nbsp    July 29, 2019</span>
                            </div>
                            <div class="blog-list-media">
                                <ul class="mid-link">
                                    <li class="mid-social-item inherit-link">
                                        <a class="social-link" href="">
                                            <i class="fab fa-facebook-f"></i>
                                        </a>
                                    </li>
                                    <li class="mid-social-item inherit-link">
                                        <a class="social-link" href="">
                                            <i class="fab fa-youtube"></i>
                                        </a>
                                    </li>
                                    <li class="mid-social-item inherit-link">
                                        <a class="social-link" class="social-link" href="">
                                            <i class="fab fa-twitter"></i>
                                        </a>
                                    </li>
                                    <li class="mid-social-item inherit-link">
                                        <a class="social-link" class="social-link" href="">
                                            <i class="fab fa-instagram"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>                        
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-12">
                <div class="next-column">
                    <div class="blog-search-list wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                        <h6 class="next-head">search</h6>
                        <div class="search-box">
                            <form class="form-inline">
                                <input class="col-xl-7 col-lg-7 col-md-12 col-12 form-control" type="search" placeholder="Search for blog" aria-label="Search">
                                <div class="btn-wrapper-search col-xl-5 col-lg-5 col-md-6 col-12">
                                    <button class="btn" type="submit">Search</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="about-author wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                        <h1 class="header-author">about author</h1>
                        <div class="author-image">
                            <img src="images/author.jpg" alt="">
                        </div>
                        <div class="author-name">
                            <span class="author-name-tag">Kierrra Astor Doe</span>
                            <p class="author-name-para">Lorem ipsum dolor sit amet, consectetur adipiscing 
                            elit, sed do eiusmod tempor incididunt aduhasvd</p>
                            <div class="read-more-btn">
                                <a href="#" class="btn after-btn">
                                    <span class="angle-button">Read more</span>
                                    <img class="angle-img" src="images/arrow-right.png" alt="">
                                </a>
                            </div>   
                        </div>                            
                    </div>
                    <div class="next-col-ad wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                        <img src="images/banner-ad.jpg" alt="">
                    </div>
                    <div class="about-news wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                        <h1 class="news-letter">newsletter</h1>
                        <div class="suscribe-newsletter">
                            <div class="news-media">
                                <ul class="news-item">
                                    <li class="news-link">
                                        <a href=""><i class="fab fa-facebook-f"></i></a>
                                    </li>
                                    <li class="news-link">
                                        <a href=""><i class="fab fa-youtube"></i></a>
                                    </li>
                                    <li class="news-link">
                                        <a href=""><i class="fab fa-twitter"></i></a>
                                    </li>
                                    <li class="news-link">
                                        <a href=""><i class="fab fa-instagram"></i></a>
                                    </li>
                                </ul>                                        
                            </div>
                            <div class="news-head">
                                <h1 class="news-header">Subscribe our newsletter and we’ll get back to you</h1>
                            </div>
                            <div class="news-form">
                                <form class="form-inline">
                                    <div class="form-group col-12">                                            
                                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Your email address.....">
                                    </div>
                                    <div class="read-more-btn">
                                        <a href="#" class="btn after-btn">
                                            <span class="angle-button">Read more</span>
                                            <img class="angle-img" src="images/arrow-right.png" alt="">
                                        </a>
                                    </div>                                             
                                </form>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
        </div>  
    </div>
</div>  