<!--footer-sec-->

<div class="footer-section-wrapper">
    <div class="container">
        <div class="row wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
            <div class="col-lg-3 col-md-4 col-sm-4 col-12">
                <div class="blog88-header">
                    <h1 class="blog88-head">blog88</h1>
                    <ul class="footer-blog">
                        <li class="footer-link">
                            <a href="" class="footer-address" href="">23 King Street, 5th Avenue, New York<br>
                                1-2355-3345-5
                            </a>
                        </li>
                        <li class="footer-link">
                            <a href="" class="footer-call" href="">call@blog88</a>
                        </li>
                    </ul>
                </div>                                                  
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4 col-12">
                <div class="foot-recent-wrap">
                    <h1 class="blog88-head">recent posts</h1>
                    <div class="recent-foot-post">
                        <ul class="footer-blog recent-foot">
                            <li class="footer-link">
                                <a href="">Food</a>
                            </li>
                            <li class="footer-link">
                                <a href="">Travel</a>
                            </li>
                            <li class="footer-link">
                                <a href="">Fashion</a>
                            </li>
                            <li class="footer-link">
                                <a href="">Trending</a>
                            </li>
                            <li class="footer-link">
                                <a href="" >Photography</a>
                            </li>
                            <li class="footer-link">
                                <a href="" >Technology</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4 col-12">
                <div class="footer-media">
                    <h1 class="blog88-head">follow us on </h1>
                    <div class="recent-foot-post">                           
                        <ul class="share-items">
                            <div class="li-wrapper">
                                <li class="share-link face-book">
                                    <a href="">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>                                           
                                </li>
                            </div>
                            <div class="li-wrapper">
                                <li class="share-link face-book">
                                    <a href="">
                                        <i class="fab fa-youtube"></i> 
                                    </a>                                           
                                </li>
                            </div>
                            <div class="li-wrapper">
                                <li class="share-link face-book">
                                    <a href="">
                                        <i class="fab fa-twitter twi-tter"></i>
                                    </a>                                           
                                </li>
                            </div>
                            <div class="li-wrapper">
                                <li class="share-link face-book">
                                    <a href="">
                                        <i class="fab fa-instagram"></i>
                                    </a>                                           
                                </li>
                            </div>
                        </ul>
                    </div>
                </div>                    
            </div> 
            <div class="col-lg-3 col-md-4 col-sm-4 col-12 news-foot">
                <div class="footer-top-wrapper">
                    <h1 class="blog88-head">news letter</h1>
                    <div class="footer-form">
                        <div class="news-form">
                            <form class="form-inline">
                                <div class="form-group col-12">                                          
                                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Your email address.....">
                                </div>
                                <div class="read-more-btn">
                                    <a href="#" class="btn after-btn">
                                        <span class="angle-button">Suscribe</span>
                                        <img class="angle-img" src="images/arrow-right.png" alt="">
                                    </a>
                                </div>                                            
                            </form>
                        </div>
                    </div>
                </div>
            </div> 
        </div>           
    </div>
</div>

    <!--footer-sec-->
  
     <!--footer-bottom-sec-->
    
    <div class="bottom-foot-wrapper">
        <div class="container">
            <div class="row wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                <div class="col-xl-8 col-lg-8 col-md-7 col-12">
                    <div class="footer-link">
                        <ul class="bottom-link">
                            <li class="footer-items">
                                <a href="" class="bottom-tag">FAQS</a>
                            </li>
                            <li class="footer-items">
                                <a href="" class="bottom-tag">Privacy Policies</a>
                            </li>
                            <li class="footer-items">
                                <a href="" class="bottom-tag">Terms and Conditions</a>
                            </li>
                            <li class="footer-items">
                                <a href="" class="bottom-tag">About</a>
                            </li>
                            <li class="footer-items">
                                <a href="" class="bottom-tag">Careers</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-5 col-12">
                    <div class="footer-last">
                        <span class="span-last"><i class="far fa-copyright"></i> 2014 Blogger Life. Design by Themewing</span>
                    </div>
                </div>
            </div>
        </div>    
    </div>    

    <!--footer-bottom-sec-->

    <!-- Java JavaScript -->
    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/app.js"></script>
</body>
</html>