<!-- header -->

<?php include("header-3.php"); ?>

<!-- header -->

 <!-- mid banner three -->

<section class="mid-carousel-wrap">
    <img class="carousel-image" src="images/hd1.jpg" alt="">
    
    <div class="mid-carousel-wrapper d-flex">
        <div class="slick-carousel layout-five-carousel">
            <div class="slider-div animated fadeInDown">
                <div class="carousel-items">
                    <div class="mid-categ">
                        <span class="" role="button">Mountaineering</span>
                    </div>
                    <div class="mid-heads">
                        <h1>Look Deep Into My Bikini and Then You’ll Understand Everything Better</h1>
                        <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna</span>
                        <div class="footer-insta-button">
                            <div class="read-more-btn">
                                <a href="#" class="btn after-btn">
                                    <span class="angle-button">Read More</span>
                                    <img class="angle-img" src="images/arrow-right.png" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="slider-div animated fadeInDown">
                <div class="carousel-items">
                    <div class="mid-categ">
                        <span class="" role="button">Mountaineering</span>
                    </div>
                    <div class="mid-heads">
                        <h1>Look Deep Into My Bikini and Then You’ll Understand Everything Better</h1>
                        <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna</span>
                        <div class="footer-insta-button">
                            <div class="read-more-btn">
                                <a href="#" class="btn after-btn">
                                    <span class="angle-button">Read More</span>
                                    <img class="angle-img" src="images/arrow-right.png" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="slider-div animated fadeInDown">
                <div class="carousel-items">
                    <div class="mid-categ">
                        <span class="" role="button">Mountaineering</span>
                    </div>
                    <div class="mid-heads">
                        <h1>Look Deep Into My Bikini and Then You’ll Understand Everything Better</h1>
                        <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna</span>
                        <div class="footer-insta-button">
                            <div class="read-more-btn">
                                <a href="#" class="btn after-btn">
                                    <span class="angle-button">Read More</span>
                                    <img class="angle-img" src="images/arrow-right.png" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="slider-div animated fadeInDown">
                <div class="carousel-items">
                    <div class="mid-categ">
                        <span class="" role="button">Mountaineering</span>
                    </div>
                    <div class="mid-heads">
                        <h1>Look Deep Into My Bikini and Then You’ll Understand Everything Better</h1>
                        <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna</span>
                        <div class="footer-insta-button">
                            <div class="read-more-btn">
                                <a href="#" class="btn after-btn">
                                    <span class="angle-button">Read More</span>
                                    <img class="angle-img" src="images/arrow-right.png" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="slider-div animated fadeInDown">
                <div class="carousel-items">
                    <div class="mid-categ">
                        <span class="" role="button">Mountaineering</span>
                    </div>
                    <div class="mid-heads">
                        <h1>Look Deep Into My Bikini and Then You’ll Understand Everything Better</h1>
                        <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna</span>
                        <div class="footer-insta-button">
                            <div class="read-more-btn">
                                <a href="#" class="btn after-btn">
                                    <span class="angle-button">Read More</span>
                                    <img class="angle-img" src="images/arrow-right.png" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="slider-div animated fadeInDown">
                <div class="carousel-items">
                    <div class="mid-categ">
                        <span class="" role="button">Mountaineering</span>
                    </div>
                    <div class="mid-heads">
                        <h1>Look Deep Into My Bikini and Then You’ll Understand Everything Better</h1>
                        <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna</span>
                        <div class="footer-insta-button">
                            <div class="read-more-btn">
                                <a href="#" class="btn after-btn">
                                    <span class="angle-button">Read More</span>
                                    <img class="angle-img" src="images/arrow-right.png" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="carousel-buttons animated fadeInDown">
            <button type="button" class="top-arrow" style="display: inline-block;">
                <i class="fas fa-angle-down"></i>
            </button>
            <button type="button" data-role="none" class="bottom-arrow" aria-label="Next" role="button" style="display: inline-block;" aria-disabled="false">
                <i class="fas fa-angle-up"></i>
            </button>
        </div>
    </div>
</section>

<!-- mid banner three -->

<!-- featured blog -->

<section class="featured-blog third-layout">
    <div class="featured-blog-wrapper">
        <h1 class="blog-head wow slideInUp" data-wow-duration="0.6s" data-wow-delay="0s">featured blogs</h1>
        <div class="container">
            <div class="blog-feature">
                <div class="row">                        
                    <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                        <div class="blog-feature-wrapper wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                            <img class="blog-feature-img" src="images/gate.jpg" alt="">
                            <div class="wrap-buttons-descp">                                
                                <div class="feature-mid">
                                    <div class="category-display">
                                        <span class="category-name">Animals</span>
                                    </div>
                                    <div class="continue-reading-btn">
                                        <a href="#" class="btn after-btn">
                                            <span class="angle-button">Read more</span>
                                            <img class="angle-img" src="images/arrow-right.png" alt="">
                                        </a>
                                    </div>
                                    <div class="blog-descp">
                                        <h5 class="">10 Most Awesome Breathtaking Places</h5>
                                        <span class="theme-by">by Spiderbuzz &nbsp &nbsp    |  &nbsp  &nbsp  july 29, 2019</span>
                                    </div>  
                                </div> 
                            </div>                                                                                                               
                        </div>                                                     
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-12 feature-col">
                        <div class="blog-feature-wrapper wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                            <img class="blog-feature-img" src="images/gate.jpg" alt="">
                            <div class="wrap-buttons-descp">                                
                                <div class="feature-mid">
                                    <div class="category-display">
                                        <span class="category-name">About Culture</span>
                                    </div>
                                    <div class="continue-reading-btn">
                                        <a href="#" class="btn after-btn">
                                            <span class="angle-button">Read more</span>
                                            <img class="angle-img" src="images/arrow-right.png" alt="">
                                        </a>
                                    </div>
                                    <div class="blog-descp">
                                        <h5 class="">10 Most Awesome Breathtaking Places</h5>
                                        <span class="theme-by">by Spiderbuzz &nbsp &nbsp    |  &nbsp  &nbsp  july 29, 2019</span>
                                    </div>  
                                </div> 
                            </div>                                                                                                               
                        </div>                                                     
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-12 feature-bot" >
                        <div class="blog-feature-wrapper wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                            <img class="blog-feature-img" src="images/food3.jpg" alt="">
                            <div class="wrap-buttons-descp">                                
                                <div class="feature-mid">
                                    <div class="category-display">
                                        <span class="category-name">About Lifestyle</span>
                                    </div>
                                    <div class="continue-reading-btn">
                                        <a href="#" class="btn after-btn">
                                            <span class="angle-button">Read more</span>
                                            <img class="angle-img" src="images/arrow-right.png" alt="">
                                        </a>
                                    </div>
                                    <div class="blog-descp">
                                        <h5 class="">10 Most Awesome Breathtaking Places</h5>
                                        <span class="theme-by">by Spiderbuzz &nbsp &nbsp    |  &nbsp  &nbsp  july 29, 2019</span>
                                    </div>  
                                </div> 
                            </div>                                                                                                               
                        </div>                                                      
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-12 feature-bot">
                        <div class="blog-feature-wrapper wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                            <img class="blog-feature-img" src="images/dance.jpg" alt="">
                            <div class="wrap-buttons-descp">                                
                                <div class="feature-mid">
                                    <div class="category-display">
                                        <span class="category-name">Animals</span>
                                    </div>
                                    <div class="continue-reading-btn">
                                        <a href="#" class="btn after-btn">
                                            <span class="angle-button">Read more</span>
                                            <img class="angle-img" src="images/arrow-right.png" alt="">
                                        </a>
                                    </div>
                                    <div class="blog-descp">
                                        <h5 class="">10 Most Awesome Breathtaking Places</h5>
                                        <span class="theme-by">by Spiderbuzz &nbsp &nbsp    |  &nbsp  &nbsp  july 29, 2019</span>
                                    </div>  
                                </div> 
                            </div>                                                                                                               
                        </div>                                                      
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- featured blog -->

<!-- body content -->

<div class="blog-content-wrapper home-page list-five">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 col-lg-8 col-md-8 col-12">
                <div class="blog-list wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                    <h6 class="next-head">blog-list</h6>
                </div>
                <div class="about-blog-section blog-top-section wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                    <div class="about-blog-wrapper">
                        <div class="blog-wrap-color">
                            <div class="row">
                                <div class="col-xl-6 col-lg-6 col-md-12 col-12 blog-wrap-col">
                                    <div class="about-img">
                                        <img src="images/gymlady.jpg" alt="">                                      
                                        <div class="like-button-list">
                                            <div class="like-button">
                                                <div class="read-more-like">
                                                    <div class="btn-like-wrap">
                                                        <div class="like-back">
                                                            <i class="fas fa-heart"></i>
                                                        </div> 
                                                    </div>  
                                                </div>                                                                           
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-12 col-12">
                                    <div class="beauty-head">
                                        <span>By Spiderbuzz &nbsp; &nbsp;    |  &nbsp;  &nbsp;  July 29, 2019</span>
                                        <h1>The Beauty Of Sunset Create A Romantic Magic In The Hearts Of
                                            Lovers and a Cute</h1>
                                        <div class="like-comment next-top">
                                            <ul class="like-link">
                                                <li class="like-item">
                                                    <i class="fas fa-heart"></i>
                                                    <span>568</span>
                                                    <a>Likes</a>
                                                </li>
                                                <li class="like-item">
                                                    <i class="fas fa-comments"></i>
                                                    <span>268</span>
                                                    <a>Comments</a>
                                                </li>
                                                <li class="like-item">
                                                    <i class="fas fa-share"></i>
                                                    <span>10</span>
                                                    <a>Shares</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <p class="display-none-para">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed 
                                            do eiusmod tempor</p>                                       
                                        <div class="read-more-btn">
                                            <a href="#" class="btn after-btn">
                                                <span class="angle-button">Read more</span>
                                                <img class="angle-img" src="images/arrow-right.png" alt="">
                                            </a>
                                        </div>                                          
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="blog-wrap-color mt-4">
                            <div class="row">
                                <div class="col-xl-6 col-lg-6 col-md-12 col-12 blog-wrap-col">
                                    <div class="about-img">
                                        <img src="images/gymlady.jpg" alt="">                                      
                                        <div class="like-button-list">
                                            <div class="like-button">
                                                <div class="read-more-like">
                                                    <div class="btn-like-wrap">
                                                        <div class="like-back">
                                                            <i class="fas fa-heart"></i>
                                                        </div> 
                                                    </div>  
                                                </div>                                                                           
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-12 col-12">
                                    <div class="beauty-head">
                                        <span>By Spiderbuzz &nbsp; &nbsp;    |  &nbsp;  &nbsp;  July 29, 2019</span>
                                        <h1>The Beauty Of Sunset Create A Romantic Magic In The Hearts Of
                                            Lovers and a Cute</h1>
                                        <div class="like-comment next-top">
                                            <ul class="like-link">
                                                <li class="like-item">
                                                    <i class="fas fa-heart"></i>
                                                    <span>568</span>
                                                    <a>Likes</a>
                                                </li>
                                                <li class="like-item">
                                                    <i class="fas fa-comments"></i>
                                                    <span>268</span>
                                                    <a>Comments</a>
                                                </li>
                                                <li class="like-item">
                                                    <i class="fas fa-share"></i>
                                                    <span>10</span>
                                                    <a>Shares</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <p class="display-none-para">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed 
                                            do eiusmod tempor</p>                                       
                                        <div class="read-more-btn">
                                            <a href="#" class="btn after-btn">
                                                <span class="angle-button">Read more</span>
                                                <img class="angle-img" src="images/arrow-right.png" alt="">
                                            </a>
                                        </div>                                          
                                    </div>
                                </div>
                            </div>
                        </div>        
                    </div>
                </div>
                <div class="blog-list-firstad wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                    <img src="images/landscape-banner-ad.jpg" alt="">
                </div>
                <div class="horizontal-blog-wrapper wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 about">
                            <div class="card">
                                <div class="card-header-sec">
                                    <img class="two-section-blog" src="images/food2.jpg" alt="">
                                    <div class="read-more-btn">
                                        <a href="#" class="btn after-btn">
                                            <span class="angle-button">Read more</span>
                                            <img class="angle-img" src="images/arrow-right.png" alt="">
                                        </a>
                                    </div>                                            
                                </div>                                    
                                <div class="card-body about-title">
                                    <span class="card-span">By Spiderbuzz &nbsp; &nbsp;    |  &nbsp;  &nbsp;  July 29, 2019</span>
                                    <h5 class="card-title">The Beauty Of Sunset Create A Romantic Magic In The Hearts of</h5>
                                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                    <div class="like-button-hori">
                                        <div class="like-back">
                                            <div class="icon-wrap">
                                                <i class="fas fa-heart"></i>
                                            </div>
                                        </div>                                               
                                    </div>                                     
                                </div>
                            </div>                                 
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 about-hori">
                            <div class="card">
                                <div class="card-header-sec">
                                    <img class="two-section-blog" src="images/food2.jpg" alt="">
                                    <div class="read-more-btn">
                                        <a href="#" class="btn after-btn">
                                            <span class="angle-button">Read more</span>
                                            <img class="angle-img" src="images/arrow-right.png" alt="">
                                        </a>
                                    </div>                                            
                                </div>                                    
                                <div class="card-body about-title ">
                                    <span class="card-span">By Spiderbuzz &nbsp; &nbsp;    |  &nbsp;  &nbsp;  July 29, 2019</span>
                                    <h5 class="card-title">The Beauty Of Sunset Create A Romantic Magic In The Hearts of</h5>
                                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut</p>
                                    <div class="like-button-hori">
                                        <div class="like-back">
                                            <div class="icon-wrap">
                                                <i class="fas fa-heart"></i>
                                            </div>
                                        </div>                                              
                                    </div>                                     
                                </div>
                            </div>                                 
                        </div>
                    </div>
                </div>

                <div class="blog-wrap-color third-layout wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-12 col-12 blog-wrap-col column-wrapper">
                            <div class="about-img">
                                <img src="images/gymlady.jpg" alt="">                                      
                                <div class="like-button-list">
                                    <div class="like-button">
                                        <div class="read-more-like">
                                            <div class="btn-like-wrap">
                                                <div class="like-back">
                                                    <i class="fas fa-heart"></i>
                                                </div> 
                                            </div>  
                                        </div>                                                                           
                                    </div>
                                </div> 
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-12 col-12 column-wrapper">
                            <div class="beauty-head">
                                <span>By Spiderbuzz &nbsp; &nbsp;    |  &nbsp;  &nbsp;  July 29, 2019</span>
                                <h1>The Beauty Of Sunset Create A Romantic Magic In The Hearts Of
                                    Lovers and a Cute</h1>
                                <div class="like-comment next-top">
                                    <ul class="like-link">
                                        <li class="like-item">
                                            <i class="fas fa-heart"></i>
                                            <span>568</span>
                                            <a>Likes</a>
                                        </li>
                                        <li class="like-item">
                                            <i class="fas fa-comments"></i>
                                            <span>268</span>
                                            <a>Comments</a>
                                        </li>
                                        <li class="like-item">
                                            <i class="fas fa-share"></i>
                                            <span>10</span>
                                            <a>Shares</a>
                                        </li>
                                    </ul>
                                </div>
                                <p class="display-none-para">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed 
                                    do eiusmod tempor</p>                                       
                                <div class="read-more-btn">
                                    <a href="#" class="btn after-btn">
                                        <span class="angle-button">Read more</span>
                                        <img class="angle-img" src="images/arrow-right.png" alt="">
                                    </a>
                                </div>                                          
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-12">
                <div class="next-column">
                    <div class="blog-search-list wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                        <h6 class="next-head">search</h6>
                        <div class="search-box">
                            <form class="form-inline">
                                <input class="col-xl-7 col-lg-7 col-md-12 col-12 form-control" type="search" placeholder="Search for blog" aria-label="Search">
                                <div class="btn-wrapper-search col-xl-5 col-lg-5 col-md-6 col-12">
                                    <button class="btn" type="submit">Search</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="about-author wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                        <h1 class="header-author">about author</h1>
                        <div class="author-image">
                            <img src="images/author.jpg" alt="">
                        </div>
                        <div class="author-name">
                            <span class="author-name-tag">Kierrra Astor Doe</span>
                            <p class="author-name-para">Lorem ipsum dolor sit amet, consectetur adipiscing 
                            elit, sed do eiusmod tempor incididunt aduhasvd</p>
                            <div class="read-more-btn">
                                <a href="#" class="btn after-btn">
                                    <span class="angle-button">Read more</span>
                                    <img class="angle-img" src="images/arrow-right.png" alt="">
                                </a>
                            </div>   
                        </div>                            
                    </div>
                    <div class="next-col-ad wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                        <img src="images/banner-ad.jpg" alt="">
                    </div>  
                    <div class="about-news wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                        <h1 class="news-letter">newsletter</h1>
                        <div class="suscribe-newsletter">
                            <div class="news-media">
                                <ul class="news-item">
                                    <li class="news-link">
                                        <a href=""><i class="fab fa-facebook-f"></i></a>
                                    </li>
                                    <li class="news-link">
                                        <a href=""><i class="fab fa-youtube"></i></a>
                                    </li>
                                    <li class="news-link">
                                        <a href=""><i class="fab fa-twitter"></i></a>
                                    </li>
                                    <li class="news-link">
                                        <a href=""><i class="fab fa-instagram"></i></a>
                                    </li>
                                </ul>                                        
                            </div>
                            <div class="news-head">
                                <h1 class="news-header">Subscribe our newsletter and we’ll get back to you</h1>
                            </div>
                            <div class="news-form">
                                <form class="form-inline">
                                    <div class="form-group col-12">                                            
                                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Your email address.....">
                                    </div>
                                    <div class="read-more-btn">
                                        <a href="#" class="btn after-btn">
                                            <span class="angle-button">Read more</span>
                                            <img class="angle-img" src="images/arrow-right.png" alt="">
                                        </a>
                                    </div>                                             
                                </form>
                            </div>
                        </div>
                    </div>                  
                </div>                   
            </div>
        </div>
    </div>
</div>

<!-- body content -->





<!-- footer -->

<section class="dark-footer-wrapper home-page-three wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
    <div class="container">
        <div class="dark-border">
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                    <ul class="footer-media">
                        <li class="footer-item">
                            <div class="footer-media-icons">
                                <a href="" class="footer-link">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </div>
                        </li>
                        <li class="footer-item youtube">
                            <div class="footer-media-icons">
                                <a href="" class="footer-link">
                                    <i class="fab fa-youtube"></i>
                                </a>
                            </div>
                        </li>
                        <li class="footer-item instagram">
                            <div class="footer-media-icons">
                                <a href="" class="footer-link">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </div>
                        </li>
                        <li class="footer-item twitter">
                            <div class="footer-media-icons">
                                <a href="" class="footer-link">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12">
                    <div class="foot-head">
                        <a href="">Blog88</a>
                    </div>
                </div>
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12">
                    <div class="footer-menu">
                        <ul class="menu-item">
                            <li class="menu">
                                <a href="" class="menu-link">FAQS</a>
                            </li>
                            <li class="menu">
                                <a href="" class="menu-link">Terms and Conditions</a></li>
                            <li class="menu">
                                <a href="" class="menu-link">About</a></li>
                            <li class="menu">
                                <a href="" class="menu-link">Privacy Policy</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>            
    </div>
</section>

<!-- footer -->


    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="library/slick/slick.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/app.js"></script>

</body>
</html>