<div class="blog-content-wrapper home-page list-five">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 col-lg-8 col-md-8 col-12">
                <div class="blog-list wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                    <h6 class="next-head">blog-list</h6>
                </div>
                <div class="about-blog-section blog-top-section wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                    <div class="about-blog-wrapper">
                        <div class="blog-wrap-color">
                            <div class="row">
                                <div class="col-xl-6 col-lg-6 col-md-12 col-12 blog-wrap-col">
                                    <div class="about-img">
                                        <img src="images/gymlady.jpg" alt="">                                      
                                        <div class="like-button-list">
                                            <div class="like-button">
                                                <div class="read-more-like">
                                                    <div class="btn-like-wrap">
                                                        <div class="like-back">
                                                            <i class="fas fa-heart"></i>
                                                        </div> 
                                                    </div>  
                                                </div>                                                                           
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-12 col-12">
                                    <div class="beauty-head">
                                        <span>By Spiderbuzz &nbsp; &nbsp;    |  &nbsp;  &nbsp;  July 29, 2019</span>
                                        <h1>The Beauty Of Sunset Create A Romantic Magic In The Hearts Of
                                            Lovers and a Cute</h1>
                                        <div class="like-comment next-top">
                                            <ul class="like-link">
                                                <li class="like-item">
                                                    <i class="fas fa-heart"></i>
                                                    <span>568</span>
                                                    <a>Likes</a>
                                                </li>
                                                <li class="like-item">
                                                    <i class="fas fa-comments"></i>
                                                    <span>268</span>
                                                    <a>Comments</a>
                                                </li>
                                                <li class="like-item">
                                                    <i class="fas fa-share"></i>
                                                    <span>10</span>
                                                    <a>Shares</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <p class="display-none-para">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed 
                                            do eiusmod tempor</p>                                       
                                        <div class="read-more-btn">
                                            <a href="#" class="btn after-btn">
                                                <span class="angle-button">Read more</span>
                                                <img class="angle-img" src="images/arrow-right.png" alt="">
                                            </a>
                                        </div>                                          
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="blog-wrap-color mt-4">
                            <div class="row">
                                <div class="col-xl-6 col-lg-6 col-md-12 col-12 blog-wrap-col">
                                    <div class="about-img">
                                        <img src="images/gymlady.jpg" alt="">                                      
                                        <div class="like-button-list">
                                            <div class="like-button">
                                                <div class="read-more-like">
                                                    <div class="btn-like-wrap">
                                                        <div class="like-back">
                                                            <i class="fas fa-heart"></i>
                                                        </div> 
                                                    </div>  
                                                </div>                                                                           
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-12 col-12">
                                    <div class="beauty-head">
                                        <span>By Spiderbuzz &nbsp; &nbsp;    |  &nbsp;  &nbsp;  July 29, 2019</span>
                                        <h1>The Beauty Of Sunset Create A Romantic Magic In The Hearts Of
                                            Lovers and a Cute</h1>
                                        <div class="like-comment next-top">
                                            <ul class="like-link">
                                                <li class="like-item">
                                                    <i class="fas fa-heart"></i>
                                                    <span>568</span>
                                                    <a>Likes</a>
                                                </li>
                                                <li class="like-item">
                                                    <i class="fas fa-comments"></i>
                                                    <span>268</span>
                                                    <a>Comments</a>
                                                </li>
                                                <li class="like-item">
                                                    <i class="fas fa-share"></i>
                                                    <span>10</span>
                                                    <a>Shares</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <p class="display-none-para">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed 
                                            do eiusmod tempor</p>                                       
                                        <div class="read-more-btn">
                                            <a href="#" class="btn after-btn">
                                                <span class="angle-button">Read more</span>
                                                <img class="angle-img" src="images/arrow-right.png" alt="">
                                            </a>
                                        </div>                                          
                                    </div>
                                </div>
                            </div>
                        </div>        
                    </div>
                </div>
                <div class="blog-list-firstad wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                    <img src="images/landscape-banner-ad.jpg" alt="">
                </div>
                <div class="horizontal-blog-wrapper wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 about">
                            <div class="card">
                                <div class="card-header-sec">
                                    <img class="two-section-blog" src="images/food2.jpg" alt="">
                                    <div class="read-more-btn">
                                        <a href="#" class="btn after-btn">
                                            <span class="angle-button">Read more</span>
                                            <img class="angle-img" src="images/arrow-right.png" alt="">
                                        </a>
                                    </div>                                            
                                </div>                                    
                                <div class="card-body about-title">
                                    <span class="card-span">By Spiderbuzz &nbsp; &nbsp;    |  &nbsp;  &nbsp;  July 29, 2019</span>
                                    <h5 class="card-title">The Beauty Of Sunset Create A Romantic Magic In The Hearts of</h5>
                                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                    <div class="like-button-hori">
                                        <div class="like-back">
                                            <div class="icon-wrap">
                                                <i class="fas fa-heart"></i>
                                            </div>
                                        </div>                                               
                                    </div>                                     
                                </div>
                            </div>                                 
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 about-hori">
                            <div class="card">
                                <div class="card-header-sec">
                                    <img class="two-section-blog" src="images/food2.jpg" alt="">
                                    <div class="read-more-btn">
                                        <a href="#" class="btn after-btn">
                                            <span class="angle-button">Read more</span>
                                            <img class="angle-img" src="images/arrow-right.png" alt="">
                                        </a>
                                    </div>                                            
                                </div>                                    
                                <div class="card-body about-title ">
                                    <span class="card-span">By Spiderbuzz &nbsp; &nbsp;    |  &nbsp;  &nbsp;  July 29, 2019</span>
                                    <h5 class="card-title">The Beauty Of Sunset Create A Romantic Magic In The Hearts of</h5>
                                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut</p>
                                    <div class="like-button-hori">
                                        <div class="like-back">
                                            <div class="icon-wrap">
                                                <i class="fas fa-heart"></i>
                                            </div>
                                        </div>                                              
                                    </div>                                     
                                </div>
                            </div>                                 
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-12">
                <div class="next-column">
                    <div class="blog-search-list wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                        <h6 class="next-head">search</h6>
                        <div class="search-box">
                            <form class="form-inline">
                                <input class="col-xl-7 col-lg-7 col-md-12 col-12 form-control" type="search" placeholder="Search for blog" aria-label="Search">
                                <div class="btn-wrapper-search col-xl-5 col-lg-5 col-md-6 col-12">
                                    <button class="btn" type="submit">Search</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="about-author wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                        <h1 class="header-author">about author</h1>
                        <div class="author-image">
                            <img src="images/author.jpg" alt="">
                        </div>
                        <div class="author-name">
                            <span class="author-name-tag">Kierrra Astor Doe</span>
                            <p class="author-name-para">Lorem ipsum dolor sit amet, consectetur adipiscing 
                            elit, sed do eiusmod tempor incididunt aduhasvd</p>
                            <div class="read-more-btn">
                                <a href="#" class="btn after-btn">
                                    <span class="angle-button">Read more</span>
                                    <img class="angle-img" src="images/arrow-right.png" alt="">
                                </a>
                            </div>   
                        </div>                            
                    </div>
                    <div class="next-col-ad wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                        <img src="images/banner-ad.jpg" alt="">
                    </div>                    
                </div>                    
            </div>
        </div>
    </div>
</div>