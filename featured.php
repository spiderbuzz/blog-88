<section class="featured-blog">
        <div class="featured-blog-wrapper">
            <h1 class="blog-head wow slideInUp" data-wow-duration="0.6s" data-wow-delay="0s">featured blogs</h1>
            <div class="container">
                <div class="blog-feature">
                    <div class="row">                        
                        <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="blog-feature-wrapper wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                                <img class="blog-feature-img" src="images/gate.jpg" alt="">
                                <div class="wrap-buttons-descp">                                
                                    <div class="feature-mid">
                                        <div class="category-display">
                                            <span class="category-name">Animals</span>
                                        </div>
                                        <div class="continue-reading-btn">
                                            <a href="#" class="btn after-btn">
                                                <span class="angle-button">Read more</span>
                                                <img class="angle-img" src="images/arrow-right.png" alt="">
                                            </a>
                                        </div>
                                        <div class="blog-descp">
                                            <h5 class="">10 Most Awesome Breathtaking Places</h5>
                                            <span class="theme-by">by Spiderbuzz &nbsp &nbsp    |  &nbsp  &nbsp  july 29, 2019</span>
                                        </div>  
                                    </div> 
                                </div>                                                                                                               
                            </div>                                                     
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-12 feature-col">
                            <div class="blog-feature-wrapper wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                                <img class="blog-feature-img" src="images/gate.jpg" alt="">
                                <div class="wrap-buttons-descp">                                
                                    <div class="feature-mid">
                                        <div class="category-display">
                                            <span class="category-name">About Culture</span>
                                        </div>
                                        <div class="continue-reading-btn">
                                            <a href="#" class="btn after-btn">
                                                <span class="angle-button">Read more</span>
                                                <img class="angle-img" src="images/arrow-right.png" alt="">
                                            </a>
                                        </div>
                                        <div class="blog-descp">
                                            <h5 class="">10 Most Awesome Breathtaking Places</h5>
                                            <span class="theme-by">by Spiderbuzz &nbsp &nbsp    |  &nbsp  &nbsp  july 29, 2019</span>
                                        </div>  
                                    </div> 
                                </div>                                                                                                               
                            </div>                                                     
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-12 feature-bot" >
                            <div class="blog-feature-wrapper wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                                <img class="blog-feature-img" src="images/food3.jpg" alt="">
                                <div class="wrap-buttons-descp">                                
                                    <div class="feature-mid">
                                        <div class="category-display">
                                            <span class="category-name">About Lifestyle</span>
                                        </div>
                                        <div class="continue-reading-btn">
                                            <a href="#" class="btn after-btn">
                                                <span class="angle-button">Read more</span>
                                                <img class="angle-img" src="images/arrow-right.png" alt="">
                                            </a>
                                        </div>
                                        <div class="blog-descp">
                                            <h5 class="">10 Most Awesome Breathtaking Places</h5>
                                            <span class="theme-by">by Spiderbuzz &nbsp &nbsp    |  &nbsp  &nbsp  july 29, 2019</span>
                                        </div>  
                                    </div> 
                                </div>                                                                                                               
                            </div>                                                      
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-12 feature-bot">
                            <div class="blog-feature-wrapper wow slideInUp" data-wow-duration="1s" data-wow-delay="0s">
                                <img class="blog-feature-img" src="images/dance.jpg" alt="">
                                <div class="wrap-buttons-descp">                                
                                    <div class="feature-mid">
                                        <div class="category-display">
                                            <span class="category-name">Animals</span>
                                        </div>
                                        <div class="continue-reading-btn">
                                            <a href="#" class="btn after-btn">
                                                <span class="angle-button">Read more</span>
                                                <img class="angle-img" src="images/arrow-right.png" alt="">
                                            </a>
                                        </div>
                                        <div class="blog-descp">
                                            <h5 class="">10 Most Awesome Breathtaking Places</h5>
                                            <span class="theme-by">by Spiderbuzz &nbsp &nbsp    |  &nbsp  &nbsp  july 29, 2019</span>
                                        </div>  
                                    </div> 
                                </div>                                                                                                               
                            </div>                                                      
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>