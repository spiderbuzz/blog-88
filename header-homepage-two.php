<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Blog-88 Homepage 2</title>

     <!--Bootstrap CSSS-->
    <link rel="stylesheet" href="css/bootstrap.min.css"> 
    <!--Google fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=DM+Serif+Display|Poppins&display|Roboto&display=swap">
    <link rel="stylesheet" href="font-awesome/css/fontawsome.css">
    <link href="https://fonts.googleapis.com/css?family=Kristi&display=swap" rel="stylesheet">
    <link rel="icon" type="text/css" href="">
    <link rel="stylesheet" href="library/owl-carousel/css/owl.carousel.min.css">  
    <link rel="stylesheet" href="css/animate.css">  
    <link rel="stylesheet" href="css/style.css">  
</head>
<body>

    <!--header starts--> 
    <header class="top-header homepage-two">
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light navbar-fixed">
                <a class="navbar-brand" href="#">Blog-88</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-icon">
                        <i class="open-nav fas fa-bars"></i>
                    </span>
                </button>
                
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Business <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="#">World </a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Fashion </a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Food </a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Style </a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Football </a>
                        </li>                 
                    </ul>
                    <form class="form-inline my-2 my-lg-0">
                        <input class="form-control top-header-search mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                        <button class="btn my-2 my-sm-0" type="submit">
                            <i class="fas fa-search"></i>
                        </button>
                    </form>
                </div>
            </nav>
        </div>        
    </header>
    <!--header ends--> 
