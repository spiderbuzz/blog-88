<!-- header -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Blog-88 Product Page</title>

     <!--Bootstrap CSSS-->
    <link rel="stylesheet" href="css/bootstrap.min.css"> 
    <!--Google fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=DM+Serif+Display|Poppins&display|Roboto&display=swap">
    <link rel="stylesheet" href="font-awesome/css/fontawsome.css">
    <link href="https://fonts.googleapis.com/css?family=Kristi&display=swap" rel="stylesheet">
    <link rel="icon" type="text/css" href="">
    <link rel="stylesheet" href="library/owl-carousel/css/owl.carousel.min.css">  
    <link rel="stylesheet" href="library/slick/slick.css">  
    <link rel="stylesheet" href="css/animate.css">  
    <link rel="stylesheet" href="css/style.css">  
</head>
<body>

    <!--header starts--> 
    <header class="top-header homepage-three fourth-layout">
        <div class="container">
            <div class="nav-top-wrapper">
                <div class="row">
                    <div class="col-4">
                        <div class="menu-wrap">
                            <a href="">
                                <img src="images/mennu-two.png" alt="">
                            </a>
                            <li class="nav-item">
                                <a class="nav-links" href="#">Menu</a>
                            </li>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="homepage-three-link">
                            <a class="navbar-brand" href="">Product Page</a>
                        </div>
                    </div>
                    <div class="col-4">
                        <form class="form-inline my-2 my-lg-0 header-four-search">
                            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                            <button class="btn my-2 my-sm-0" type="submit">
                                <i class="fas fa-search"></i>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div> 
        <div class="nav-wrapper-top">
            <div class="container">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <div class="dialer-section homepage-four-dialer">
                        <li class="dial-icon">
                            <a href="" class="dialer-icon">
                                <i class="fas fa-phone"></i>
                            </a>
                        </li>
                        <li class="dial-icon number">
                            <a href="">call@blog88</a>
                        </li>
                    </div>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav homepage-navbar mr-auto ml-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="#">Business <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">World </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Fashion </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Food </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Style </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Football </a>
                            </li>                 
                        </ul>
                        <ul class="footer-links-list top-header-four">
                            <li class="footer-list d-inline-block ml-1">
                                <div class="footer-two-icons">
                                    <a href="" class="footer-icon-link active">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </div>
                            </li>
                            <li class="footer-list d-inline-block ml-1">
                                <div class="footer-two-icons">
                                    <a href="" class="footer-icon-link">
                                        <i class="fab fa-youtube"></i>
                                    </a>
                                </div>
                            </li>
                            <li class="footer-list d-inline-block ml-1">
                                <div class="footer-two-icons">
                                    <a href="" class="footer-icon-link">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </div>
                            </li>
                            <li class="footer-list d-inline-block ml-1">
                                <div class="footer-two-icons">
                                    <a href="" class="footer-icon-link">
                                        <i class="fab fa-instagram"></i>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
                <div class="fifth-header-layout">
                    <div class="homepage-three-link">
                        <a class="navbar-brand" href="">Blog88</a>
                    </div>
                </div>
            </div>
        </div>       
    </header>

<!-- header -->

<!-- banner -->

<div id="carouselExampleCaptions" class="carousel slide product-page" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
        <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="product-banner-img" src="images/pro.jpg" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block">
                <div class="mid-banner-content animated fadeInDown">
                    <div class="mid-categ">
                        <span class="" role="button">Best Offers</span>
                    </div>
                    <div class="mid-heads">
                        <h1> Latest Iphone 11 Pro on 13% Discount </h1>
                        <span>Hurry Up Before the offer Ends</span>
                    </div>
                    <div class="mid-social-media">
                        <div class="read-more-btn">
                            <a href="#" class="btn after-btn">
                                <span class="angle-button">Buy Now</span>
                                <img class="angle-img" src="images/arrow-right.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <img class="product-banner-img" src="images/rog.jpg" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block">
                <div class="mid-banner-content animated fadeInDown">
                    <div class="mid-categ">
                        <span class="" role="button">Best Offers</span>
                    </div>
                    <div class="mid-heads">
                        <h1> Latest Gaming Mobile Phone on 10% Discount </h1>
                        <span>Hurry Up Before the offer Ends</span>
                    </div>
                    <div class="mid-social-media">
                        <div class="read-more-btn">
                            <a href="#" class="btn after-btn">
                                <span class="angle-button">Buy Now</span>
                                <img class="angle-img" src="images/arrow-right.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <img class="product-banner-img" src="images/joy.jpg" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block">
                <div class="mid-banner-content animated fadeInDown">
                    <div class="mid-categ">
                        <span class="" role="button">Best Offers</span>
                    </div>
                    <div class="mid-heads">
                        <h1> Gaming Apparels on 25% Discount </h1>
                        <span>Hurry Up Before the offer Ends</span>
                    </div>
                    <div class="mid-social-media">
                        <div class="read-more-btn">
                            <a href="#" class="btn after-btn">
                                <span class="angle-button">Buy Now</span>
                                <img class="angle-img" src="images/arrow-right.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <img class="product-banner-img" src="images/joy.jpg" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block">
                <div class="mid-banner-content animated fadeInDown">
                    <div class="mid-categ">
                        <span class="" role="button">Best Offers</span>
                    </div>
                    <div class="mid-heads">
                        <h1> Gaming Apparels on 25% Discount </h1>
                        <span>Hurry Up Before the offer Ends</span>
                    </div>
                    <div class="mid-social-media">
                        <div class="read-more-btn">
                            <a href="#" class="btn after-btn">
                                <span class="angle-button">Buy Now</span>
                                <img class="angle-img" src="images/arrow-right.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="carousel-controllers">
        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
            <div class="carousel-control">
                <i class="fas fa-angle-left"></i>
            </div>
        </a>
        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
            <div class="carousel-control">
                <i class="fas fa-angle-right"></i>
            </div>
        </a>
    </div>
</div>

<!-- banner -->

<!-- catagories -->

<section class="production-catagory">
    <div class="container">
        <div class="row">
            <div class="col-4"></div>
        </div>
    </div>
</section>

<!-- catagories -->

<!-- footer -->

<?php include("footer.php"); ?>

<!-- footer -->
